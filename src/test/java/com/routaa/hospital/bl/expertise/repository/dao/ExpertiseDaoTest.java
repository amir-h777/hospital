package com.routaa.hospital.bl.expertise.repository.dao;

import com.routaa.hospital.entity.ExpertiseEntity;
import com.routaa.hospital.entity.HospitalEntity;
import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class ExpertiseDaoTest {

    private ExpertiseEntity expertiseEntity;

    private ExpertiseDao expertiseDao;

    @Autowired
    public ExpertiseDaoTest(ExpertiseDao expertiseDao){
        this.expertiseDao = expertiseDao;
    }

    @BeforeEach
    public void setUp(){
        expertiseEntity = new ExpertiseEntity();
        expertiseEntity.setName("TestExpertise");
    }

    @Test
    public void givenExpertise_whenSave_thenReturnSavedExpertise(){
        ExpertiseEntity saved = expertiseDao.save(expertiseEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getId());
        });
    }

    @Test
    public void givenExpertise_whenUpdate_thenReturnUpdatedExpertise(){
        ExpertiseEntity saved = expertiseDao.save(expertiseEntity);
        saved.setName("UpdatedExpertise");
        ExpertiseEntity updated = expertiseDao.save(saved);

        Assertions.assertAll(() -> {
            Assertions.assertEquals("UpdatedExpertise", updated.getName());
            Assertions.assertEquals(updated.getId(), saved.getId());
        });
    }

    @Test
    public void givenExpertiseId_whenFindById_thenReturnExpertise(){
        ExpertiseEntity saved = expertiseDao.save(expertiseEntity);
        ExpertiseEntity found = expertiseDao.findById(saved.getId()).get();

        Assertions.assertNotNull(found);
    }

    @Test
    public void whenFindAll_thenReturnExpertises(){
        expertiseDao.save(expertiseEntity);
        expertiseDao.save(expertiseEntity);
        Iterable<ExpertiseEntity> iterable = expertiseDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnExpertisesCount(){
        expertiseDao.save(expertiseEntity);
        expertiseDao.save(expertiseEntity);
        long count = expertiseDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenExpertisesId_whenDeleteById(){
        ExpertiseEntity saved = expertiseDao.save(expertiseEntity);

        Assertions.assertNotNull(saved);

        expertiseDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),expertiseDao.findById(saved.getId()));
    }
}
