package com.routaa.hospital.bl.expertise.repository.service;

import com.routaa.hospital.bl.expertise.model.ExpertiseDtoIn;
import com.routaa.hospital.bl.expertise.model.ExpertiseDtoOut;
import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.model.HospitalDtoOut;
import com.routaa.hospital.entity.ExpertiseEntity;
import com.routaa.hospital.entity.HospitalEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ExpertiseServiceTest {

    @Mock
    private ExpertiseDao expertiseDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ExpertiseService expertiseService;

    private ExpertiseEntity expertiseEntity;
    private ExpertiseDtoIn expertiseDtoIn;

    @BeforeEach
    public void setUp(){
        expertiseEntity = new ExpertiseEntity();
        expertiseEntity.setId(1);
        expertiseEntity.setName("TestExpertise");
        expertiseDtoIn = new ExpertiseDtoIn(expertiseEntity);
    }

    @Test
    public void givenExpertise_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(expertiseDtoIn, ExpertiseEntity.class)).thenReturn(expertiseEntity);
        Mockito.when(expertiseDao.save(expertiseEntity)).thenReturn(expertiseEntity);

        ExpertiseDtoOut dtoOut = expertiseService.create(expertiseDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });

    }

    @Test
    public void givenExpertise_whenUpdate_thenReturnUpdatedDtoOut(){

        expertiseEntity.setName("UpdatedExpertise");
        expertiseDtoIn = new ExpertiseDtoIn(expertiseEntity);

        Mockito.when(modelMapper.map(expertiseDtoIn, ExpertiseEntity.class)).thenReturn(expertiseEntity);
        Mockito.when(expertiseDao.save(expertiseEntity)).thenReturn(expertiseEntity);
        Mockito.when(expertiseDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(expertiseEntity));

        ExpertiseDtoOut updatedDtoOut = expertiseService.update(1, expertiseDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals("UpdatedExpertise", updatedDtoOut.getName());
        });

    }

    @Test
    public void givenExpertiseId_whenGetById_thenReturnDtoOut(){
        Mockito.when(expertiseDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(expertiseEntity));

        ExpertiseDtoOut foundDtoOut = expertiseService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(expertiseDao.findAll()).thenReturn(List.of(expertiseEntity));

        List<ExpertiseDtoOut> dtoOutList = expertiseService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(expertiseDao.count()).thenReturn(1L);

        long count = expertiseService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenExpertiseId_whenDeleteById(){
        Mockito.doNothing().when(expertiseDao).deleteById(Mockito.any(Integer.class));

        expertiseService.deleteById(1);

        Mockito.verify(expertiseDao, Mockito.times(1)).deleteById(1);

    }

}
