package com.routaa.hospital.bl.expertise.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.expertise.model.ExpertiseDtoIn;
import com.routaa.hospital.bl.expertise.model.ExpertiseDtoOut;
import com.routaa.hospital.bl.expertise.repository.service.ExpertiseService;
import com.routaa.hospital.bl.hospital.model.HospitalDtoIn;
import com.routaa.hospital.bl.hospital.model.HospitalDtoOut;
import com.routaa.hospital.entity.ExpertiseEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ExpertiseController.class)
public class ExpertiseControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ExpertiseService expertiseService;

    private ExpertiseEntity expertiseEntity;
    private ExpertiseDtoIn expertiseDtoIn;
    private ExpertiseDtoOut expertiseDtoOut;

    @BeforeEach
    public void setUp(){

        expertiseEntity = new ExpertiseEntity();
        expertiseEntity.setId(1);
        expertiseEntity.setName("TestExpertise");

        expertiseDtoIn = new ExpertiseDtoIn(expertiseEntity);
        expertiseDtoOut = new ExpertiseDtoOut(expertiseEntity);
    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(expertiseService.create(Mockito.any(ExpertiseDtoIn.class))).thenReturn(expertiseDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/expertise")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(expertiseDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("TestExpertise"));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtOut() throws Exception{

        expertiseEntity.setName("UpdatedExpertise");
        expertiseDtoIn = new ExpertiseDtoIn(expertiseEntity);
        expertiseDtoOut = new ExpertiseDtoOut(expertiseEntity);

        Mockito.when(expertiseService.update(Mockito.any(Integer.class), Mockito.any(ExpertiseDtoIn.class))).thenReturn(expertiseDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/expertise/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(expertiseDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("UpdatedExpertise"));

    }

    @Test
    public void givenExpertiseId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(expertiseService.getById(Mockito.any(Integer.class))).thenReturn(expertiseDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/expertise/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("TestExpertise"));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(expertiseService.getAll()).thenReturn(List.of(expertiseDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/expertises")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(expertiseService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/expertises/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenExpertiseId_whenDelete() throws Exception{

        Mockito.doNothing().when(expertiseService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/expertise/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
