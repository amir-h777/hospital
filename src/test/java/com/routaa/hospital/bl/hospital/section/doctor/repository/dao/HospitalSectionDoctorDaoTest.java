package com.routaa.hospital.bl.hospital.section.doctor.repository.dao;

import com.routaa.hospital.bl.doctor.repository.dao.DoctorDao;
import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.bl.hospital.section.repository.dao.HospitalSectionDao;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class HospitalSectionDoctorDaoTest {

    private HospitalSectionDoctorEntity hospitalSectionDoctorEntity;

    private HospitalSectionDoctorDao hospitalSectionDoctorDao;
    private HospitalSectionDao hospitalSectionDao;
    private HospitalTypeDao hospitalTypeDao;
    private HospitalDao hospitalDao;
    private ExpertiseDao expertiseDao;
    private DoctorDao doctorDao;

    @Autowired
    public HospitalSectionDoctorDaoTest(HospitalSectionDoctorDao hospitalSectionDoctorDao
            , HospitalSectionDao hospitalSectionDao, HospitalTypeDao hospitalTypeDao
            , HospitalDao hospitalDao, ExpertiseDao expertiseDao, DoctorDao doctorDao){

        this.hospitalSectionDoctorDao = hospitalSectionDoctorDao;
        this.hospitalSectionDao = hospitalSectionDao;
        this.hospitalTypeDao = hospitalTypeDao;
        this.hospitalDao = hospitalDao;
        this.expertiseDao = expertiseDao;
        this.doctorDao = doctorDao;
    }

    public void preSetUp(){
        HospitalTypeEntity hospitalType = new HospitalTypeEntity();
        HospitalTypeEntity saved = hospitalTypeDao.save(hospitalType);

        HospitalEntity hospital = new HospitalEntity();
        hospital.setHospitalTypeId(saved.getId());
        HospitalEntity saved1 = hospitalDao.save(hospital);

        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved2 = expertiseDao.save(expertise);

        HospitalSectionEntity hospitalSection = new HospitalSectionEntity();
        hospitalSection.setHospitalId(saved1.getId());
        hospitalSection.setExpertiseId(saved2.getId());
        HospitalSectionEntity saved3 = hospitalSectionDao.save(hospitalSection);

        DoctorEntity doctor = new DoctorEntity();
        doctor.setExpertiseId(saved2.getId());
        DoctorEntity saved4 = doctorDao.save(doctor);

        hospitalSectionDoctorEntity.setHospitalSectionId(saved3.getId());
        hospitalSectionDoctorEntity.setDoctorId(saved4.getId());
    }

    @BeforeEach
    public void setUp(){
        hospitalSectionDoctorEntity = new HospitalSectionDoctorEntity();
        preSetUp();
    }

    @Test
    public void givenHospitalSectionDoctor_whenSave_thenReturnSavedHospitalSectionDoctor(){
        HospitalSectionDoctorEntity saved = hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getHospitalSectionId());
            Assertions.assertNotEquals(0,saved.getDoctorId());
        });
    }

    @Test
    public void whenFindAll_thenReturnHospitalSectionDoctors(){
        hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity);
        hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity);
        Iterable<HospitalSectionDoctorEntity> iterable = hospitalSectionDoctorDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnHospitalSectionDoctorsCount(){
        hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity);
        hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity);
        long count = hospitalSectionDoctorDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenHospitalSectionDoctorId_whenDeleteById(){
        HospitalSectionDoctorEntity saved = hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity);

        Assertions.assertNotNull(saved);

        hospitalSectionDoctorDao.deleteByHospitalSectionIdAndDoctorId(saved.getHospitalSectionId(), saved.getDoctorId());

        Assertions.assertEquals(Optional.empty(),hospitalSectionDoctorDao
                .findByHospitalSectionIdAndDoctorId(saved.getHospitalSectionId(), saved.getDoctorId()));
    }

}
