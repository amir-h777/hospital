package com.routaa.hospital.bl.hospital.expertise.repository.service;

import com.routaa.hospital.bl.hospital.expertise.model.HospitalExpertiseDtoIn;
import com.routaa.hospital.bl.hospital.expertise.model.HospitalExpertiseDtoOut;
import com.routaa.hospital.bl.hospital.expertise.repository.dao.HospitalExpertiseDao;
import com.routaa.hospital.bl.hospital.section.model.HospitalSectionDtoOut;
import com.routaa.hospital.entity.HospitalExpertiseEntity;
import com.routaa.hospital.entity.HospitalSectionEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class HospitalExpertiseServiceTest {

    @Mock
    private HospitalExpertiseDao hospitalExpertiseDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private HospitalExpertiseService hospitalExpertiseService;

    private HospitalExpertiseEntity hospitalExpertiseEntity;
    private HospitalExpertiseDtoIn hospitalExpertiseDtoIn;

    @BeforeEach
    public void setUp(){
        hospitalExpertiseEntity = new HospitalExpertiseEntity();
        hospitalExpertiseEntity.setHospitalId(1);
        hospitalExpertiseEntity.setExpertiseId(2);

        hospitalExpertiseDtoIn = new HospitalExpertiseDtoIn(hospitalExpertiseEntity);
    }

    @Test
    public void givenHospitalExpertise_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(hospitalExpertiseDtoIn, HospitalExpertiseEntity.class)).thenReturn(hospitalExpertiseEntity);
        Mockito.when(hospitalExpertiseDao.save(hospitalExpertiseEntity)).thenReturn(hospitalExpertiseEntity);

        HospitalExpertiseDtoOut dtoOut = hospitalExpertiseService.create(hospitalExpertiseDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getHospitalId());
            Assertions.assertEquals(2, dtoOut.getExpertiseId());
        });

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(hospitalExpertiseDao.findAll()).thenReturn(List.of(hospitalExpertiseEntity));

        List<HospitalExpertiseDtoOut> dtoOutList = hospitalExpertiseService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(hospitalExpertiseDao.count()).thenReturn(1L);

        long count = hospitalExpertiseService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenHospitalExpertiseId_whenDeleteById(){
        Mockito.doNothing().when(hospitalExpertiseDao).deleteByHospitalIdAndExpertiseId(Mockito.any(Integer.class), Mockito.any(Integer.class));

        hospitalExpertiseService.deleteById(1, 2);

        Mockito.verify(hospitalExpertiseDao, Mockito.times(1)).deleteByHospitalIdAndExpertiseId(1, 2);

    }

}
