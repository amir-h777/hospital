package com.routaa.hospital.bl.hospital.section.repository.dao;

import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.ExpertiseEntity;
import com.routaa.hospital.entity.HospitalEntity;
import com.routaa.hospital.entity.HospitalSectionEntity;
import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class HospitalSectionDaoTest {

    private HospitalSectionEntity hospitalSectionEntity;

    private HospitalSectionDao hospitalSectionDao;
    private HospitalTypeDao hospitalTypeDao;
    private HospitalDao hospitalDao;
    private ExpertiseDao expertiseDao;

    @Autowired
    public HospitalSectionDaoTest(HospitalSectionDao hospitalSectionDao, HospitalTypeDao hospitalTypeDao
            , HospitalDao hospitalDao, ExpertiseDao expertiseDao){
        this.hospitalSectionDao = hospitalSectionDao;
        this.hospitalTypeDao = hospitalTypeDao;
        this.hospitalDao = hospitalDao;
        this.expertiseDao = expertiseDao;
    }

    public void preSetUp(){
        HospitalTypeEntity hospitalType = new HospitalTypeEntity();
        HospitalTypeEntity saved = hospitalTypeDao.save(hospitalType);

        HospitalEntity hospital = new HospitalEntity();
        hospital.setHospitalTypeId(saved.getId());
        HospitalEntity saved1 = hospitalDao.save(hospital);

        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved2 = expertiseDao.save(expertise);

        hospitalSectionEntity.setHospitalId(saved1.getId());
        hospitalSectionEntity.setExpertiseId(saved2.getId());

    }

    @BeforeEach
    public void setUp(){
        hospitalSectionEntity = new HospitalSectionEntity();
        hospitalSectionEntity.setName("TestHospitalSection");
        preSetUp();
    }

    @Test
    public void givenHospitalSection_whenSave_thenReturnSavedHospitalSection(){
        HospitalSectionEntity saved = hospitalSectionDao.save(hospitalSectionEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getId());
        });
    }

    @Test
    public void givenHospitalSection_whenUpdate_thenReturnUpdatedHospitalSection(){
        HospitalSectionEntity saved = hospitalSectionDao.save(hospitalSectionEntity);
        saved.setName("UpdatedHospitalSection");
        HospitalSectionEntity updated = hospitalSectionDao.save(saved);

        Assertions.assertAll(() -> {
            Assertions.assertEquals("UpdatedHospitalSection", updated.getName());
            Assertions.assertEquals(updated.getId(), saved.getId());
        });
    }

    @Test
    public void givenHospitalSectionId_whenFindById_thenReturnHospitalSection(){
        HospitalSectionEntity saved = hospitalSectionDao.save(hospitalSectionEntity);
        HospitalSectionEntity found = hospitalSectionDao.findById(saved.getId()).get();

        Assertions.assertNotNull(found);
    }

    @Test
    public void whenFindAll_thenReturnHospitalSections(){
        hospitalSectionDao.save(hospitalSectionEntity);
        hospitalSectionDao.save(hospitalSectionEntity);
        Iterable<HospitalSectionEntity> iterable = hospitalSectionDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnHospitalSectionsCount(){
        hospitalSectionDao.save(hospitalSectionEntity);
        hospitalSectionDao.save(hospitalSectionEntity);
        long count = hospitalSectionDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenHospitalSectionId_whenDeleteById(){
        HospitalSectionEntity saved = hospitalSectionDao.save(hospitalSectionEntity);

        Assertions.assertNotNull(saved);

        hospitalSectionDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),hospitalSectionDao.findById(saved.getId()));
    }

}
