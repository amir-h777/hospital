package com.routaa.hospital.bl.hospital.section.repository.service;

import com.routaa.hospital.bl.hospital.section.model.HospitalSectionDtoIn;
import com.routaa.hospital.bl.hospital.section.model.HospitalSectionDtoOut;
import com.routaa.hospital.bl.hospital.section.repository.dao.HospitalSectionDao;
import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoOut;
import com.routaa.hospital.entity.HospitalSectionEntity;
import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class HospitalSectionServiceTest {

    @Mock
    private HospitalSectionDao hospitalSectionDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private HospitalSectionService hospitalSectionService;

    private HospitalSectionEntity hospitalSectionEntity;
    private HospitalSectionDtoIn hospitalSectionDtoIn;

    @BeforeEach
    public void setup(){
        hospitalSectionEntity = new HospitalSectionEntity();
        hospitalSectionEntity.setId(1);
        hospitalSectionEntity.setHospitalId(2);
        hospitalSectionEntity.setExpertiseId(3);
        hospitalSectionEntity.setName("TestHospitalSection");

        hospitalSectionDtoIn = new HospitalSectionDtoIn(hospitalSectionEntity);
    }

    @Test
    public void givenHospitalSection_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(hospitalSectionDtoIn, HospitalSectionEntity.class)).thenReturn(hospitalSectionEntity);
        Mockito.when(hospitalSectionDao.save(hospitalSectionEntity)).thenReturn(hospitalSectionEntity);

        HospitalSectionDtoOut dtoOut = hospitalSectionService.create(hospitalSectionDtoIn);

        Assertions.assertAll(() ->{
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });
    }

    @Test
    public void givenHospitalSection_whenUpdate_thenReturnUpdatedDtoOut(){

        hospitalSectionEntity.setName("UpdatedHospitalSection");
        hospitalSectionDtoIn = new HospitalSectionDtoIn(hospitalSectionEntity);

        Mockito.when(modelMapper.map(hospitalSectionDtoIn, HospitalSectionEntity.class)).thenReturn(hospitalSectionEntity);
        Mockito.when(hospitalSectionDao.save(hospitalSectionEntity)).thenReturn(hospitalSectionEntity);
        Mockito.when(hospitalSectionDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(hospitalSectionEntity));

        HospitalSectionDtoOut updatedDtoOut = hospitalSectionService.update(1, hospitalSectionDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals("UpdatedHospitalSection", updatedDtoOut.getName());
        });

    }

    @Test
    public void givenHospitalSectionId_whenGetById_thenReturnDtoOut(){
        Mockito.when(hospitalSectionDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(hospitalSectionEntity));

        HospitalSectionDtoOut foundDtoOut = hospitalSectionService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(hospitalSectionDao.findAll()).thenReturn(List.of(hospitalSectionEntity));

        List<HospitalSectionDtoOut> dtoOutList = hospitalSectionService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(hospitalSectionDao.count()).thenReturn(1L);

        long count = hospitalSectionService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenHospitalSectionId_whenDeleteById(){
        Mockito.doNothing().when(hospitalSectionDao).deleteById(Mockito.any(Integer.class));

        hospitalSectionService.deleteById(1);

        Mockito.verify(hospitalSectionDao, Mockito.times(1)).deleteById(1);

    }

}
