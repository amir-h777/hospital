package com.routaa.hospital.bl.hospital.type.repository.service;

import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoIn;
import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoOut;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class HospitalTypeServiceTest {

    @Mock
    private HospitalTypeDao hospitalTypeDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private HospitalTypeService hospitalTypeService;

    private HospitalTypeEntity hospitalTypeEntity;
    private HospitalTypeDtoIn hospitalTypeDtoIn;

    @BeforeEach
    public void setUp(){
        hospitalTypeEntity = new HospitalTypeEntity();
        hospitalTypeEntity.setId(1);
        hospitalTypeEntity.setName("TestHospitalType");

        hospitalTypeDtoIn = new HospitalTypeDtoIn(hospitalTypeEntity);
    }

    @Test
    public void givenHospitalType_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(hospitalTypeDtoIn, HospitalTypeEntity.class)).thenReturn(hospitalTypeEntity);
        Mockito.when(hospitalTypeDao.save(hospitalTypeEntity)).thenReturn(hospitalTypeEntity);

        HospitalTypeDtoOut dtoOut = hospitalTypeService.create(hospitalTypeDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });
    }

    @Test
    public void givenHospitalType_whenUpdate_thenReturnUpdatedDtoOut(){

        hospitalTypeEntity.setName("UpdatedHospitalType");
        hospitalTypeDtoIn = new HospitalTypeDtoIn(hospitalTypeEntity);

        Mockito.when(modelMapper.map(hospitalTypeDtoIn, HospitalTypeEntity.class)).thenReturn(hospitalTypeEntity);
        Mockito.when(hospitalTypeDao.save(hospitalTypeEntity)).thenReturn(hospitalTypeEntity);
        Mockito.when(hospitalTypeDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(hospitalTypeEntity));

        HospitalTypeDtoOut updatedDtoOut = hospitalTypeService.update(1, hospitalTypeDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals("UpdatedHospitalType", updatedDtoOut.getName());
        });

    }

    @Test
    public void givenHospitalTypeId_whenGetById_thenReturnDtoOut(){
        Mockito.when(hospitalTypeDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(hospitalTypeEntity));

        HospitalTypeDtoOut foundDtoOut = hospitalTypeService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(hospitalTypeDao.findAll()).thenReturn(List.of(hospitalTypeEntity));

        List<HospitalTypeDtoOut> dtoOutList = hospitalTypeService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(hospitalTypeDao.count()).thenReturn(1L);

        long count = hospitalTypeService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenHospitalTypeId_whenDelete(){
        Mockito.doNothing().when(hospitalTypeDao).deleteById(Mockito.any(Integer.class));

        hospitalTypeService.deleteById(1);

        Mockito.verify(hospitalTypeDao, Mockito.times(1)).deleteById(1);

    }

}
