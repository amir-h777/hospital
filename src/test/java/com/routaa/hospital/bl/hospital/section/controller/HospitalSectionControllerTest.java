package com.routaa.hospital.bl.hospital.section.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.expertise.model.ExpertiseDtoIn;
import com.routaa.hospital.bl.expertise.model.ExpertiseDtoOut;
import com.routaa.hospital.bl.hospital.section.model.HospitalSectionDtoIn;
import com.routaa.hospital.bl.hospital.section.model.HospitalSectionDtoOut;
import com.routaa.hospital.bl.hospital.section.repository.service.HospitalSectionService;
import com.routaa.hospital.entity.HospitalSectionEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HospitalSectionController.class)
public class HospitalSectionControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HospitalSectionService hospitalSectionService;

    private HospitalSectionEntity hospitalSectionEntity;
    private HospitalSectionDtoIn hospitalSectionDtoIn;
    private HospitalSectionDtoOut hospitalSectionDtoOut;

    @BeforeEach
    public void setUp(){

        hospitalSectionEntity = new HospitalSectionEntity();
        hospitalSectionEntity.setId(1);
        hospitalSectionEntity.setHospitalId(2);
        hospitalSectionEntity.setExpertiseId(3);
        hospitalSectionEntity.setName("TestHospitalSection");

        hospitalSectionDtoIn = new HospitalSectionDtoIn(hospitalSectionEntity);
        hospitalSectionDtoOut = new HospitalSectionDtoOut(hospitalSectionEntity);
    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(hospitalSectionService.create(Mockito.any(HospitalSectionDtoIn.class))).thenReturn(hospitalSectionDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/hospital-section")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalSectionDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.expertiseId").value(3))
                .andExpect(jsonPath("$.name").value("TestHospitalSection"));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        hospitalSectionEntity.setName("UpdatedHospitalSection");
        hospitalSectionDtoIn = new HospitalSectionDtoIn(hospitalSectionEntity);
        hospitalSectionDtoOut = new HospitalSectionDtoOut(hospitalSectionEntity);

        Mockito.when(hospitalSectionService.update(Mockito.any(Integer.class), Mockito.any(HospitalSectionDtoIn.class))).thenReturn(hospitalSectionDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/hospital-section/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalSectionDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("UpdatedHospitalSection"));

    }

    @Test
    public void givenHospitalSectionId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(hospitalSectionService.getById(Mockito.any(Integer.class))).thenReturn(hospitalSectionDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/hospital-section/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.expertiseId").value(3))
                .andExpect(jsonPath("$.name").value("TestHospitalSection"));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(hospitalSectionService.getAll()).thenReturn(List.of(hospitalSectionDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/hospital-sections")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(hospitalSectionService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/hospital-sections/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenHospitalSectionId_whenDelete() throws Exception{

        Mockito.doNothing().when(hospitalSectionService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/hospital-section/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
