package com.routaa.hospital.bl.hospital.doctor.repository.dao;

import com.routaa.hospital.bl.doctor.repository.dao.DoctorDao;
import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class HospitalDoctorDaoTest {

    private HospitalDoctorEntity hospitalDoctorEntity;

    private HospitalDoctorDao hospitalDoctorDao;
    private DoctorDao doctorDao;
    private ExpertiseDao expertiseDao;
    private HospitalDao hospitalDao;
    private HospitalTypeDao hospitalTypeDao;

    @Autowired
    public HospitalDoctorDaoTest(HospitalDoctorDao hospitalDoctorDao, DoctorDao doctorDao
            , ExpertiseDao expertiseDao, HospitalDao hospitalDao, HospitalTypeDao hospitalTypeDao){

        this.hospitalDoctorDao = hospitalDoctorDao;
        this.doctorDao = doctorDao;
        this.expertiseDao = expertiseDao;
        this.hospitalDao = hospitalDao;
        this.hospitalTypeDao = hospitalTypeDao;

    }

    public void preSetUp(){
        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved = expertiseDao.save(expertise);

        DoctorEntity doctor = new DoctorEntity();
        doctor.setExpertiseId(saved.getId());
        DoctorEntity saved1 = doctorDao.save(doctor);

        HospitalTypeEntity hospitalType = new HospitalTypeEntity();
        HospitalTypeEntity saved2 = hospitalTypeDao.save(hospitalType);

        HospitalEntity hospital = new HospitalEntity();
        hospital.setHospitalTypeId(saved2.getId());
        HospitalEntity saved3 = hospitalDao.save(hospital);

        hospitalDoctorEntity.setHospitalId(saved3.getId());
        hospitalDoctorEntity.setDoctorId(saved1.getId());
    }

    @BeforeEach
    public void setUp(){
        hospitalDoctorEntity = new HospitalDoctorEntity();
        preSetUp();
    }

    @Test
    public void givenHospitalDoctor_whenSave_thenReturnSavedHospitalDoctor(){
        HospitalDoctorEntity saved = hospitalDoctorDao.save(hospitalDoctorEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getHospitalId());
            Assertions.assertNotEquals(0,saved.getDoctorId());
        });
    }

    @Test
    public void whenFindAll_thenReturnHospitalDoctors(){
        hospitalDoctorDao.save(hospitalDoctorEntity);
        hospitalDoctorDao.save(hospitalDoctorEntity);
        Iterable<HospitalDoctorEntity> iterable = hospitalDoctorDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnHospitalDoctorsCount(){
        hospitalDoctorDao.save(hospitalDoctorEntity);
        hospitalDoctorDao.save(hospitalDoctorEntity);
        long count = hospitalDoctorDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenHospitalDoctorId_whenDeleteById(){
        HospitalDoctorEntity saved = hospitalDoctorDao.save(hospitalDoctorEntity);

        Assertions.assertNotNull(saved);

        hospitalDoctorDao.deleteByHospitalIdAndDoctorId(saved.getHospitalId(), saved.getDoctorId());

        Assertions.assertEquals(Optional.empty(),hospitalDoctorDao
                .findByHospitalIdAndDoctorId(saved.getHospitalId(), saved.getDoctorId()));
    }

}
