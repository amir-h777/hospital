package com.routaa.hospital.bl.hospital.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.hospital.model.HospitalDtoIn;
import com.routaa.hospital.bl.hospital.model.HospitalDtoOut;
import com.routaa.hospital.bl.hospital.repository.service.HospitalService;
import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoIn;
import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoOut;
import com.routaa.hospital.entity.HospitalEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HospitalController.class)
public class HospitalControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HospitalService hospitalService;

    private HospitalEntity hospitalEntity;
    private HospitalDtoIn hospitalDtoIn;
    private HospitalDtoOut hospitalDtoOut;

    @BeforeEach
    public void setUp(){
        hospitalEntity = new HospitalEntity();
        hospitalEntity.setId(1);
        hospitalEntity.setHospitalTypeId(2);
        hospitalEntity.setName("TestHospital");
        hospitalEntity.setAddress("TestAddress");
        hospitalEntity.setCeo("TestCeo");

        hospitalDtoIn = new HospitalDtoIn(hospitalEntity);
        hospitalDtoOut = new HospitalDtoOut(hospitalEntity);
    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(hospitalService.create(Mockito.any(HospitalDtoIn.class))).thenReturn(hospitalDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/hospital")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalTypeId").value(2))
                .andExpect(jsonPath("$.name").value("TestHospital"));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        hospitalEntity.setName("UpdatedHospital");
        hospitalDtoIn = new HospitalDtoIn(hospitalEntity);
        hospitalDtoOut = new HospitalDtoOut(hospitalEntity);

        Mockito.when(hospitalService.update(Mockito.any(Integer.class), Mockito.any(HospitalDtoIn.class))).thenReturn(hospitalDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/hospital/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("UpdatedHospital"));

    }

    @Test
    public void givenHospitalId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(hospitalService.getById(Mockito.any(Integer.class))).thenReturn(hospitalDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/hospital/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalTypeId").value(2))
                .andExpect(jsonPath("$.name").value("TestHospital"));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(hospitalService.getAll()).thenReturn(List.of(hospitalDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/hospitals")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(hospitalService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/hospitals/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenHospitalId_whenDelete() throws Exception{

        Mockito.doNothing().when(hospitalService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/hospital/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
