package com.routaa.hospital.bl.hospital.section.doctor.repository.service;

import com.routaa.hospital.bl.hospital.doctor.model.HospitalDoctorDtoOut;
import com.routaa.hospital.bl.hospital.section.doctor.model.HospitalSectionDoctorDtoIn;
import com.routaa.hospital.bl.hospital.section.doctor.model.HospitalSectionDoctorDtoOut;
import com.routaa.hospital.bl.hospital.section.doctor.repository.dao.HospitalSectionDoctorDao;
import com.routaa.hospital.entity.HospitalDoctorEntity;
import com.routaa.hospital.entity.HospitalSectionDoctorEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class HospitalSectionDoctorServiceTest {

    @Mock
    private HospitalSectionDoctorDao hospitalSectionDoctorDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private HospitalSectionDoctorService hospitalSectionDoctorService;

    private HospitalSectionDoctorEntity hospitalSectionDoctorEntity;
    private HospitalSectionDoctorDtoIn hospitalSectionDoctorDtoIn;

    @BeforeEach
    public void setUp(){
        hospitalSectionDoctorEntity = new HospitalSectionDoctorEntity();
        hospitalSectionDoctorEntity.setHospitalSectionId(1);
        hospitalSectionDoctorEntity.setDoctorId(2);

        hospitalSectionDoctorDtoIn = new HospitalSectionDoctorDtoIn(hospitalSectionDoctorEntity);
    }

    @Test
    public void givenHospitalSectionDoctor_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(hospitalSectionDoctorDtoIn, HospitalSectionDoctorEntity.class)).thenReturn(hospitalSectionDoctorEntity);
        Mockito.when(hospitalSectionDoctorDao.save(hospitalSectionDoctorEntity)).thenReturn(hospitalSectionDoctorEntity);

        HospitalSectionDoctorDtoOut dtoOut = hospitalSectionDoctorService.create(hospitalSectionDoctorDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getHospitalSectionId());
            Assertions.assertEquals(2, dtoOut.getDoctorId());
        });

    }

    @Test
    public void whenGetAll_thenReturnDtoOut(){
        Mockito.when(hospitalSectionDoctorDao.findAll()).thenReturn(List.of(hospitalSectionDoctorEntity));

        List<HospitalSectionDoctorDtoOut> dtoOutList = hospitalSectionDoctorService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(hospitalSectionDoctorDao.count()).thenReturn(1L);

        long count = hospitalSectionDoctorService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenHospitalSectionDoctorId_whenDeleteById(){
        Mockito.doNothing().when(hospitalSectionDoctorDao).deleteByHospitalSectionIdAndDoctorId(Mockito.any(Integer.class), Mockito.any(Integer.class));

        hospitalSectionDoctorService.deleteById(1, 2);

        Mockito.verify(hospitalSectionDoctorDao, Mockito.times(1)).deleteByHospitalSectionIdAndDoctorId(1, 2);

    }

}
