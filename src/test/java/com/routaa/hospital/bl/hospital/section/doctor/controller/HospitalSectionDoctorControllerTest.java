package com.routaa.hospital.bl.hospital.section.doctor.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.hospital.doctor.model.HospitalDoctorDtoIn;
import com.routaa.hospital.bl.hospital.section.doctor.model.HospitalSectionDoctorDtoIn;
import com.routaa.hospital.bl.hospital.section.doctor.model.HospitalSectionDoctorDtoOut;
import com.routaa.hospital.bl.hospital.section.doctor.repository.service.HospitalSectionDoctorService;
import com.routaa.hospital.entity.HospitalSectionDoctorEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HospitalSectionDoctorController.class)
public class HospitalSectionDoctorControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HospitalSectionDoctorService hospitalSectionDoctorService;

    private HospitalSectionDoctorEntity hospitalSectionDoctorEntity;
    private HospitalSectionDoctorDtoIn hospitalSectionDoctorDtoIn;
    private HospitalSectionDoctorDtoOut hospitalSectionDoctorDtoOut;

    @BeforeEach
    public void setUp(){

        hospitalSectionDoctorEntity = new HospitalSectionDoctorEntity();
        hospitalSectionDoctorEntity.setHospitalSectionId(1);
        hospitalSectionDoctorEntity.setDoctorId(2);

        hospitalSectionDoctorDtoIn = new HospitalSectionDoctorDtoIn(hospitalSectionDoctorEntity);
        hospitalSectionDoctorDtoOut = new HospitalSectionDoctorDtoOut(hospitalSectionDoctorEntity);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(hospitalSectionDoctorService.create(Mockito.any(HospitalSectionDoctorDtoIn.class))).thenReturn(hospitalSectionDoctorDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/doctor-in-hospital-section")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalSectionDoctorDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.hospitalSectionId").value(1))
                .andExpect(jsonPath("$.doctorId").value(2));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(hospitalSectionDoctorService.getAll()).thenReturn(List.of(hospitalSectionDoctorDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/doctors-in-hospital-sections")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(hospitalSectionDoctorService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/doctors-in-hospital-sections/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenHospitalSectionDoctorId_whenDelete() throws Exception{

        Mockito.doNothing().when(hospitalSectionDoctorService).deleteById(Mockito.any(Integer.class), Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/doctor-in-hospital-section/{id}/{id1}", 1, 2)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
