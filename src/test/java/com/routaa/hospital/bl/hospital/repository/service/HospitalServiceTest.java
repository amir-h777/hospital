package com.routaa.hospital.bl.hospital.repository.service;

import com.routaa.hospital.bl.hospital.model.HospitalDtoIn;
import com.routaa.hospital.bl.hospital.model.HospitalDtoOut;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.entity.HospitalEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class HospitalServiceTest {

    @Mock
    private HospitalDao hospitalDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private HospitalService hospitalService;

    private HospitalEntity hospitalEntity;
    private HospitalDtoIn hospitalDtoIn;

    @BeforeEach
    public void setUp(){
        hospitalEntity = new HospitalEntity();
        hospitalEntity.setId(1);
        hospitalEntity.setHospitalTypeId(2);
        hospitalEntity.setName("TestHospital");
        hospitalEntity.setAddress("TestAddress");
        hospitalEntity.setCeo("TestCeo");

        hospitalDtoIn = new HospitalDtoIn(hospitalEntity);
    }

    @Test
    public void givenHospital_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(hospitalDtoIn, HospitalEntity.class)).thenReturn(hospitalEntity);
        Mockito.when(hospitalDao.save(hospitalEntity)).thenReturn(hospitalEntity);

        HospitalDtoOut dtoOut = hospitalService.create(hospitalDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });

    }

    @Test
    public void givenHospital_whenUpdate_thenReturnUpdatedDtoOut(){

        hospitalEntity.setName("UpdatedHospital");
        hospitalDtoIn = new HospitalDtoIn(hospitalEntity);

        Mockito.when(modelMapper.map(hospitalDtoIn, HospitalEntity.class)).thenReturn(hospitalEntity);
        Mockito.when(hospitalDao.save(hospitalEntity)).thenReturn(hospitalEntity);
        Mockito.when(hospitalDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(hospitalEntity));

        HospitalDtoOut updatedDtoOut = hospitalService.update(1, hospitalDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals("UpdatedHospital", updatedDtoOut.getName());
        });

    }

    @Test
    public void givenHospitalId_whenGetById_thenReturnDtoOut(){
        Mockito.when(hospitalDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(hospitalEntity));

        HospitalDtoOut foundDtoOut = hospitalService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(hospitalDao.findAll()).thenReturn(List.of(hospitalEntity));

        List<HospitalDtoOut> dtoOutList = hospitalService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(hospitalDao.count()).thenReturn(1L);

        long count = hospitalService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenHospitalId_whenDeleteById(){
        Mockito.doNothing().when(hospitalDao).deleteById(Mockito.any(Integer.class));

        hospitalService.deleteById(1);

        Mockito.verify(hospitalDao, Mockito.times(1)).deleteById(1);

    }

}
