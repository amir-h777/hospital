package com.routaa.hospital.bl.hospital.type.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoIn;
import com.routaa.hospital.bl.hospital.type.model.HospitalTypeDtoOut;
import com.routaa.hospital.bl.hospital.type.repository.service.HospitalTypeService;
import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@WebMvcTest(controllers = HospitalTypeController.class)
public class HospitalTypeControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HospitalTypeService hospitalTypeService;


    private HospitalTypeEntity hospitalTypeEntity;
    private HospitalTypeDtoIn hospitalTypeDtoIn;
    private HospitalTypeDtoOut hospitalTypeDtoOut;


    @BeforeEach
    public void setUp(){

        hospitalTypeEntity = new HospitalTypeEntity();
        hospitalTypeEntity.setId(1);
        hospitalTypeEntity.setName("TestHospitalType");

        hospitalTypeDtoIn = new HospitalTypeDtoIn(hospitalTypeEntity);
        hospitalTypeDtoOut = new HospitalTypeDtoOut(hospitalTypeEntity);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(hospitalTypeService.create(Mockito.any(HospitalTypeDtoIn.class))).thenReturn(hospitalTypeDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/hospital-type")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalTypeDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("TestHospitalType"));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        hospitalTypeEntity.setName("UpdatedHospitalType");
        hospitalTypeDtoIn = new HospitalTypeDtoIn(hospitalTypeEntity);
        hospitalTypeDtoOut = new HospitalTypeDtoOut(hospitalTypeEntity);

        Mockito.when(hospitalTypeService.update(Mockito.any(Integer.class), Mockito.any(HospitalTypeDtoIn.class))).thenReturn(hospitalTypeDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/hospital-type/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalTypeDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("UpdatedHospitalType"));

    }

    @Test
    public void givenHospitalTypeId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(hospitalTypeService.getById(Mockito.any(Integer.class))).thenReturn(hospitalTypeDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/hospital-type/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("TestHospitalType"));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(hospitalTypeService.getAll()).thenReturn(List.of(hospitalTypeDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/hospital-types")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(hospitalTypeService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/hospital-types/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenHospitalTypeId_whenDelete() throws Exception{

        Mockito.doNothing().when(hospitalTypeService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/hospital-type/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
