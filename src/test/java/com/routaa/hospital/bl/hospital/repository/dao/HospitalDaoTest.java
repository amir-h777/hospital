package com.routaa.hospital.bl.hospital.repository.dao;

import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.HospitalEntity;
import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class HospitalDaoTest {

    private HospitalEntity hospitalEntity;

    private HospitalDao hospitalDao;
    private HospitalTypeDao hospitalTypeDao;

    @Autowired
    public HospitalDaoTest(HospitalDao hospitalDao, HospitalTypeDao hospitalTypeDao){
        this.hospitalDao = hospitalDao;
        this.hospitalTypeDao = hospitalTypeDao;
    }

    public void preSetUp(){
        HospitalTypeEntity hospitalTypeEntity = new HospitalTypeEntity();
        HospitalTypeEntity saved = hospitalTypeDao.save(hospitalTypeEntity);

        hospitalEntity.setHospitalTypeId(saved.getId());
    }

    @BeforeEach
    public void setUp(){
        hospitalEntity = new HospitalEntity();
        hospitalEntity.setName("TestHospital");
        hospitalEntity.setAddress("address");
        hospitalEntity.setCeo("ceo");
        preSetUp();
    }

    @Test
    public void givenHospital_whenSave_thenReturnSavedHospital(){
        HospitalEntity saved = hospitalDao.save(hospitalEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getId());
        });
    }

    @Test
    public void givenHospital_whenUpdate_thenReturnUpdatedHospital(){
        HospitalEntity saved = hospitalDao.save(hospitalEntity);
        saved.setName("UpdatedHospital");
        HospitalEntity updated = hospitalDao.save(saved);

        Assertions.assertAll(() -> {
            Assertions.assertEquals("UpdatedHospital", updated.getName());
            Assertions.assertEquals(updated.getId(), saved.getId());
        });
    }

    @Test
    public void givenHospitalId_whenFindById_thenReturnHospital(){
        HospitalEntity saved = hospitalDao.save(hospitalEntity);
        HospitalEntity found = hospitalDao.findById(saved.getId()).get();

        Assertions.assertNotNull(found);
    }

    @Test
    public void whenFindAll_thenReturnHospitals(){
        hospitalDao.save(hospitalEntity);
        hospitalDao.save(hospitalEntity);
        Iterable<HospitalEntity> iterable = hospitalDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnHospitalsCount(){
        hospitalDao.save(hospitalEntity);
        hospitalDao.save(hospitalEntity);
        long count = hospitalDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenHospitalId_whenDeleteById(){
        HospitalEntity saved = hospitalDao.save(hospitalEntity);

        Assertions.assertNotNull(saved);

        hospitalDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),hospitalDao.findById(saved.getId()));
    }
}
