package com.routaa.hospital.bl.hospital.doctor.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.hospital.doctor.model.HospitalDoctorDtoIn;
import com.routaa.hospital.bl.hospital.doctor.model.HospitalDoctorDtoOut;
import com.routaa.hospital.bl.hospital.doctor.repository.service.HospitalDoctorService;
import com.routaa.hospital.bl.hospital.expertise.model.HospitalExpertiseDtoIn;
import com.routaa.hospital.entity.HospitalDoctorEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HospitalDoctorController.class)
public class HospitalDoctorControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HospitalDoctorService hospitalDoctorService;

    private HospitalDoctorEntity hospitalDoctorEntity;
    private HospitalDoctorDtoIn hospitalDoctorDtoIn;
    private HospitalDoctorDtoOut hospitalDoctorDtoOut;

    @BeforeEach
    public void setUp(){

        hospitalDoctorEntity = new HospitalDoctorEntity();
        hospitalDoctorEntity.setHospitalId(1);
        hospitalDoctorEntity.setDoctorId(2);

        hospitalDoctorDtoIn = new HospitalDoctorDtoIn(hospitalDoctorEntity);
        hospitalDoctorDtoOut = new HospitalDoctorDtoOut(hospitalDoctorEntity);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(hospitalDoctorService.create(Mockito.any(HospitalDoctorDtoIn.class))).thenReturn(hospitalDoctorDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/doctor-in-hospital")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalDoctorDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.hospitalId").value(1))
                .andExpect(jsonPath("$.doctorId").value(2));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(hospitalDoctorService.getAll()).thenReturn(List.of(hospitalDoctorDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/doctors-in-hospitals")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(hospitalDoctorService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/doctors-in-hospitals/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenHospitalDoctorId_whenDelete() throws Exception{

        Mockito.doNothing().when(hospitalDoctorService).deleteById(Mockito.any(Integer.class), Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/doctor-in-hospital/{id}/{id1}", 1, 2)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
