package com.routaa.hospital.bl.hospital.expertise.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.hospital.expertise.model.HospitalExpertiseDtoIn;
import com.routaa.hospital.bl.hospital.expertise.model.HospitalExpertiseDtoOut;
import com.routaa.hospital.bl.hospital.expertise.repository.service.HospitalExpertiseService;
import com.routaa.hospital.entity.HospitalExpertiseEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HospitalExpertiseController.class)
public class HospitalExpertiseControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HospitalExpertiseService hospitalExpertiseService;

    private HospitalExpertiseEntity hospitalExpertiseEntity;
    private HospitalExpertiseDtoIn hospitalExpertiseDtoIn;
    private HospitalExpertiseDtoOut hospitalExpertiseDtoOut;

    @BeforeEach
    public void setUp(){

        hospitalExpertiseEntity = new HospitalExpertiseEntity();
        hospitalExpertiseEntity.setHospitalId(1);
        hospitalExpertiseEntity.setExpertiseId(2);

        hospitalExpertiseDtoIn = new HospitalExpertiseDtoIn(hospitalExpertiseEntity);
        hospitalExpertiseDtoOut = new HospitalExpertiseDtoOut(hospitalExpertiseEntity);
    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(hospitalExpertiseService.create(Mockito.any(HospitalExpertiseDtoIn.class))).thenReturn(hospitalExpertiseDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/expertise-in-hospital")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(hospitalExpertiseDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.hospitalId").value(1))
                .andExpect(jsonPath("$.expertiseId").value(2));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(hospitalExpertiseService.getAll()).thenReturn(List.of(hospitalExpertiseDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/expertises-in-hospitals")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(hospitalExpertiseService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/expertises-in-hospitals/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenHospitalExpertiseId_whenDelete() throws Exception{

        Mockito.doNothing().when(hospitalExpertiseService).deleteById(Mockito.any(Integer.class), Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/expertise-in-hospital/{id}/{id1}", 1, 2)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
