package com.routaa.hospital.bl.hospital.expertise.repository.dao;

import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class HospitalExpertiseDaoTest {

    private HospitalExpertiseEntity hospitalExpertiseEntity;

    private HospitalExpertiseDao hospitalExpertiseDao;
    private ExpertiseDao expertiseDao;
    private HospitalDao hospitalDao;
    private HospitalTypeDao hospitalTypeDao;

    @Autowired
    public HospitalExpertiseDaoTest(HospitalExpertiseDao hospitalExpertiseDao, ExpertiseDao expertiseDao
            , HospitalDao hospitalDao, HospitalTypeDao hospitalTypeDao){

        this.hospitalExpertiseDao = hospitalExpertiseDao;
        this.expertiseDao = expertiseDao;
        this.hospitalDao = hospitalDao;
        this.hospitalTypeDao = hospitalTypeDao;

    }

    public void preSetUp(){
        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved = expertiseDao.save(expertise);

        HospitalTypeEntity hospitalType = new HospitalTypeEntity();
        HospitalTypeEntity saved1 = hospitalTypeDao.save(hospitalType);

        HospitalEntity hospital = new HospitalEntity();
        hospital.setHospitalTypeId(saved1.getId());
        HospitalEntity saved2 = hospitalDao.save(hospital);

        hospitalExpertiseEntity.setExpertiseId(saved.getId());
        hospitalExpertiseEntity.setHospitalId(saved2.getId());
    }

    @BeforeEach
    public void setUp(){
        hospitalExpertiseEntity = new HospitalExpertiseEntity();
        preSetUp();
    }

    @Test
    public void givenHospitalExpertise_whenSave_thenReturnSavedHospitalExpertise(){
        HospitalExpertiseEntity saved = hospitalExpertiseDao.save(hospitalExpertiseEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getExpertiseId());
            Assertions.assertNotEquals(0,saved.getHospitalId());
        });
    }

    @Test
    public void whenFindAll_thenReturnHospitalExpertises(){
        hospitalExpertiseDao.save(hospitalExpertiseEntity);
        hospitalExpertiseDao.save(hospitalExpertiseEntity);
        Iterable<HospitalExpertiseEntity> iterable = hospitalExpertiseDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnHospitalExpertisesCount(){
        hospitalExpertiseDao.save(hospitalExpertiseEntity);
        hospitalExpertiseDao.save(hospitalExpertiseEntity);
        long count = hospitalExpertiseDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenHospitalExpertiseId_whenDeleteById(){
        HospitalExpertiseEntity saved = hospitalExpertiseDao.save(hospitalExpertiseEntity);

        Assertions.assertNotNull(saved);

        hospitalExpertiseDao.deleteByHospitalIdAndExpertiseId(saved.getHospitalId(), saved.getExpertiseId());

        Assertions.assertEquals(Optional.empty(),hospitalExpertiseDao
                .findByHospitalIdAndExpertiseId(saved.getHospitalId(), saved.getExpertiseId()));
    }

}
