package com.routaa.hospital.bl.hospital.type.repository.dao;

import com.routaa.hospital.entity.HospitalTypeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class HospitalTypeDaoTest {

    private HospitalTypeEntity hospitalTypeEntity;

    private HospitalTypeDao hospitalTypeDao;

    @Autowired
    public HospitalTypeDaoTest(HospitalTypeDao hospitalTypeDao){
        this.hospitalTypeDao = hospitalTypeDao;
    }

    @BeforeEach
    public void setUp(){
        hospitalTypeEntity = new HospitalTypeEntity();
        hospitalTypeEntity.setName("TestHospitalType");
    }

    @Test
    public void givenHospitalType_whenSave_thenReturnSavedHospitalType(){

        HospitalTypeEntity save = hospitalTypeDao.save(hospitalTypeEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(save);
            Assertions.assertNotEquals(0, save.getId());
        });
    }

    @Test
    public void givenHospitalType_whenUpdate_thenReturnUpdatedHospitalType(){

        HospitalTypeEntity save = hospitalTypeDao.save(hospitalTypeEntity);
        save.setName("updatedTestHospitalType");
        HospitalTypeEntity updated = hospitalTypeDao.save(save);

        Assertions.assertAll(() -> {
            Assertions.assertEquals("updatedTestHospitalType", updated.getName());
            Assertions.assertEquals(save.getId(), updated.getId());
        });
    }

    @Test
    public void givenHospitalType_whenFindById_thenReturnHospitalType(){

        HospitalTypeEntity hospitalType = hospitalTypeDao.save(hospitalTypeEntity);
        HospitalTypeEntity found = hospitalTypeDao.findById(hospitalType.getId()).get();

        Assertions.assertNotNull(found);

    }

    @Test
    public void whenFindAll_thenReturnHospitalTypes(){

        hospitalTypeDao.save(hospitalTypeEntity);
        hospitalTypeDao.save(hospitalTypeEntity);
        Iterable<HospitalTypeEntity> iterable = hospitalTypeDao.findAll();

        Assertions.assertNotNull(iterable);

    }

    @Test
    public void whenCount_thenReturnHospitalTypesCount(){
        hospitalTypeDao.save(hospitalTypeEntity);
        hospitalTypeDao.save(hospitalTypeEntity);
        long count = hospitalTypeDao.count();

        Assertions.assertNotEquals(0,count);

    }

    @Test
    public void givenHospitalTypeId_whenDeleteById(){
        HospitalTypeEntity saved = hospitalTypeDao.save(hospitalTypeEntity);

        Assertions.assertNotNull(saved);

        hospitalTypeDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),hospitalTypeDao.findById(saved.getId()));
    }
}
