package com.routaa.hospital.bl.hospital.doctor.repository.service;

import com.routaa.hospital.bl.hospital.doctor.model.HospitalDoctorDtoIn;
import com.routaa.hospital.bl.hospital.doctor.model.HospitalDoctorDtoOut;
import com.routaa.hospital.bl.hospital.doctor.repository.dao.HospitalDoctorDao;
import com.routaa.hospital.bl.hospital.expertise.model.HospitalExpertiseDtoOut;
import com.routaa.hospital.entity.HospitalDoctorEntity;
import com.routaa.hospital.entity.HospitalExpertiseEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class HospitalDoctorServiceTest {

    @Mock
    private HospitalDoctorDao hospitalDoctorDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private HospitalDoctorService hospitalDoctorService;

    private HospitalDoctorEntity hospitalDoctorEntity;
    private HospitalDoctorDtoIn hospitalDoctorDtoIn;

    @BeforeEach
    public void setUp(){
        hospitalDoctorEntity = new HospitalDoctorEntity();
        hospitalDoctorEntity.setHospitalId(1);
        hospitalDoctorEntity.setDoctorId(2);

        hospitalDoctorDtoIn = new HospitalDoctorDtoIn(hospitalDoctorEntity);
    }

    @Test
    public void givenHospitalDoctor_whenCreate_thenReturnSavedDtoOut(){

        Mockito.when(modelMapper.map(hospitalDoctorDtoIn, HospitalDoctorEntity.class)).thenReturn(hospitalDoctorEntity);
        Mockito.when(hospitalDoctorDao.save(hospitalDoctorEntity)).thenReturn(hospitalDoctorEntity);

        HospitalDoctorDtoOut dtoOut = hospitalDoctorService.create(hospitalDoctorDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getHospitalId());
            Assertions.assertEquals(2, dtoOut.getDoctorId());
        });

    }

    @Test
    public void whenGetAll_thenReturnDtoOut(){
        Mockito.when(hospitalDoctorDao.findAll()).thenReturn(List.of(hospitalDoctorEntity));

        List<HospitalDoctorDtoOut> dtoOutList = hospitalDoctorService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(hospitalDoctorDao.count()).thenReturn(1L);

        long count = hospitalDoctorService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenHospitalDoctorId_whenDeleteById(){
        Mockito.doNothing().when(hospitalDoctorDao).deleteByHospitalIdAndDoctorId(Mockito.any(Integer.class), Mockito.any(Integer.class));

        hospitalDoctorService.deleteById(1, 2);

        Mockito.verify(hospitalDoctorDao, Mockito.times(1)).deleteByHospitalIdAndDoctorId(1, 2);

    }

}
