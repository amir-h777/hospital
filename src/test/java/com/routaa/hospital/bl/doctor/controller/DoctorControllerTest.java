package com.routaa.hospital.bl.doctor.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.doctor.model.DoctorDtoIn;
import com.routaa.hospital.bl.doctor.model.DoctorDtoOut;
import com.routaa.hospital.bl.doctor.repository.service.DoctorService;
import com.routaa.hospital.entity.DoctorEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DoctorController.class)
public class DoctorControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DoctorService doctorService;

    private DoctorEntity SAVED_DOCTOR_ENTITY;
    private DoctorDtoIn SAVED_DOCTOR_DTO_IN;
    private DoctorDtoOut SAVED_DOCTOR_DTO_OUT;
    private DoctorEntity UPDATED_DOCTOR_ENTITY;
    private DoctorDtoIn UPDATED_DOCTOR_DTO_IN;
    private DoctorDtoOut UPDATED_DOCTOR_DTO_OUT;

    @BeforeEach
    public void setUp(){

        SAVED_DOCTOR_ENTITY = new DoctorEntity();
        SAVED_DOCTOR_ENTITY.setId(1);
        SAVED_DOCTOR_ENTITY.setExpertiseId(2);
        SAVED_DOCTOR_ENTITY.setFirstName("TestDoctor");
        SAVED_DOCTOR_ENTITY.setLastName("TestDoctor");

        SAVED_DOCTOR_DTO_IN = new DoctorDtoIn(SAVED_DOCTOR_ENTITY);
        SAVED_DOCTOR_DTO_OUT = new DoctorDtoOut(SAVED_DOCTOR_ENTITY);

        UPDATED_DOCTOR_ENTITY = new DoctorEntity();
        UPDATED_DOCTOR_ENTITY.setId(1);
        UPDATED_DOCTOR_ENTITY.setExpertiseId(2);
        UPDATED_DOCTOR_ENTITY.setFirstName("UpdatedDoctor");
        UPDATED_DOCTOR_ENTITY.setLastName("UpdatedDoctor");

        UPDATED_DOCTOR_DTO_IN = new DoctorDtoIn(UPDATED_DOCTOR_ENTITY);
        UPDATED_DOCTOR_DTO_OUT = new DoctorDtoOut(UPDATED_DOCTOR_ENTITY);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(doctorService.create(Mockito.any(DoctorDtoIn.class))).thenReturn(SAVED_DOCTOR_DTO_OUT);

        ResultActions resultActions = mockMvc.perform(post("/doctor")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(SAVED_DOCTOR_DTO_IN)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(SAVED_DOCTOR_DTO_OUT.getId()))
                .andExpect(jsonPath("$.expertiseId").value(SAVED_DOCTOR_DTO_OUT.getExpertiseId()))
                .andExpect(jsonPath("$.firstName").value(SAVED_DOCTOR_DTO_OUT.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(SAVED_DOCTOR_DTO_OUT.getLastName()));

        Mockito.verify(doctorService, Mockito.times(1))
                .create(Mockito.any(DoctorDtoIn.class));
        Mockito.verifyNoMoreInteractions(doctorService);

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        Mockito.when(doctorService.update(Mockito.any(Integer.class), Mockito.any(DoctorDtoIn.class)))
                .thenReturn(UPDATED_DOCTOR_DTO_OUT);

        ResultActions resultActions = mockMvc.perform(put("/doctor/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(UPDATED_DOCTOR_DTO_IN)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(SAVED_DOCTOR_DTO_OUT.getId()))
                .andExpect(jsonPath("$.expertiseId").value(SAVED_DOCTOR_DTO_OUT.getExpertiseId()))
                .andExpect(jsonPath("$.firstName").value("UpdatedDoctor"))
                .andExpect(jsonPath("$.lastName").value("UpdatedDoctor"));


        Mockito.verify(doctorService, Mockito.times(1))
                .update(Mockito.any(Integer.class), Mockito.any(DoctorDtoIn.class));
        Mockito.verifyNoMoreInteractions(doctorService);
    }

    @Test
    public void givenDoctorId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(doctorService.getById(Mockito.any(Integer.class))).thenReturn(SAVED_DOCTOR_DTO_OUT);

        ResultActions resultActions = mockMvc.perform(get("/doctor/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.expertiseId").value(2))
                .andExpect(jsonPath("$.firstName").value("TestDoctor"))
                .andExpect(jsonPath("$.lastName").value("TestDoctor"));

        Mockito.verify(doctorService, Mockito.times(1))
                .getById(Mockito.any(Integer.class));
        Mockito.verifyNoMoreInteractions(doctorService);
    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(doctorService.getAll()).thenReturn(List.of(SAVED_DOCTOR_DTO_OUT));

        ResultActions resultActions = mockMvc.perform(get("/doctors")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$.[0].id").value(1))
                .andExpect(jsonPath("$.[0].expertiseId").value(2))
                .andExpect(jsonPath("$.[0].firstName").value("TestDoctor"))
                .andExpect(jsonPath("$.[0].lastName").value("TestDoctor"));

        Mockito.verify(doctorService, Mockito.times(1))
                .getAll();
        Mockito.verifyNoMoreInteractions(doctorService);

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(doctorService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/doctors/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

        Mockito.verify(doctorService, Mockito.times(1))
                .count();
        Mockito.verifyNoMoreInteractions(doctorService);

    }

    @Test
    public void givenDoctorId_whenDelete() throws Exception{

        Mockito.doNothing().when(doctorService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/doctor/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());

        Mockito.verify(doctorService, Mockito.times(1))
                .deleteById(Mockito.any(Integer.class));
        Mockito.verifyNoMoreInteractions(doctorService);
    }

}
