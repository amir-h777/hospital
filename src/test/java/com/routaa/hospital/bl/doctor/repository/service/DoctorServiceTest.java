package com.routaa.hospital.bl.doctor.repository.service;

import com.routaa.hospital.bl.doctor.model.DoctorDtoIn;
import com.routaa.hospital.bl.doctor.model.DoctorDtoOut;
import com.routaa.hospital.bl.doctor.repository.dao.DoctorDao;
import com.routaa.hospital.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class DoctorServiceTest {

    @Mock
    private DoctorDao doctorDao;

    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private DoctorService doctorService;

    private DoctorEntity SAVED_DOCTOR_ENTITY;
    private DoctorEntity UPDATED_DOCTOR_ENTITY;
    private DoctorDtoIn DOCTOR_DTO_IN;
    private DoctorDtoOut SAVED_DOCTOR_DTO_OUT;
    private DoctorDtoOut UPDATED_DOCTOR_DTO_OUT;


    @BeforeEach
    public void setUp() {
        SAVED_DOCTOR_ENTITY = new DoctorEntity();
        SAVED_DOCTOR_ENTITY.setId(1);
        SAVED_DOCTOR_ENTITY.setExpertiseId(2);
        SAVED_DOCTOR_ENTITY.setFirstName("TestDoctor");
        SAVED_DOCTOR_ENTITY.setLastName("TestDoctor");

        DOCTOR_DTO_IN = new DoctorDtoIn(SAVED_DOCTOR_ENTITY);
        SAVED_DOCTOR_DTO_OUT = new DoctorDtoOut(SAVED_DOCTOR_ENTITY);

        UPDATED_DOCTOR_ENTITY = new DoctorEntity();
        UPDATED_DOCTOR_ENTITY.setId(1);
        UPDATED_DOCTOR_ENTITY.setExpertiseId(2);
        UPDATED_DOCTOR_ENTITY.setFirstName("UpdatedDoctor");
        UPDATED_DOCTOR_ENTITY.setLastName("UpdatedDoctor");

        UPDATED_DOCTOR_DTO_OUT = new DoctorDtoOut(UPDATED_DOCTOR_ENTITY);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnSavedDtoOut() {

        Mockito.when(doctorDao.save(Mockito.any(DoctorEntity.class))).thenReturn(SAVED_DOCTOR_ENTITY);

        DoctorDtoOut savedDoctorDtoOut = doctorService.create(DOCTOR_DTO_IN);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(savedDoctorDtoOut);
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getId(), savedDoctorDtoOut.getId());
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getExpertiseId(), savedDoctorDtoOut.getExpertiseId());
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getFirstName(), savedDoctorDtoOut.getFirstName());
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getLastName(), savedDoctorDtoOut.getLastName());
        });

        Mockito.verify(doctorDao, Mockito.times(1))
                .save(Mockito.any(DoctorEntity.class));
        Mockito.verify(modelMapper, Mockito.times(1))
                .map(DOCTOR_DTO_IN, DoctorEntity.class);
        Mockito.verifyNoMoreInteractions(doctorDao, modelMapper);
    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnUpdatedDtoOut() {

        Mockito.when(doctorDao.findById(Mockito.any(Integer.class)))
                .thenReturn(Optional.of(SAVED_DOCTOR_ENTITY));
        Mockito.when(doctorDao.save(Mockito.any(DoctorEntity.class))).thenReturn(UPDATED_DOCTOR_ENTITY);

        DoctorDtoOut updatedDoctorDtoOut = doctorService.update(1, DOCTOR_DTO_IN);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(updatedDoctorDtoOut);
            Assertions.assertEquals(UPDATED_DOCTOR_DTO_OUT.getId(), updatedDoctorDtoOut.getId());
            Assertions.assertEquals(UPDATED_DOCTOR_DTO_OUT.getExpertiseId(), updatedDoctorDtoOut.getExpertiseId());
            Assertions.assertEquals(UPDATED_DOCTOR_DTO_OUT.getFirstName(), updatedDoctorDtoOut.getFirstName());
            Assertions.assertEquals(UPDATED_DOCTOR_DTO_OUT.getLastName(), updatedDoctorDtoOut.getLastName());
        });

        Mockito.verify(doctorDao, Mockito.times(1))
                .save(Mockito.any(DoctorEntity.class));
        Mockito.verify(doctorDao, Mockito.times(1)).
                findById(Mockito.any(Integer.class));
        Mockito.verify(modelMapper, Mockito.times(1))
                .map(DOCTOR_DTO_IN, DoctorEntity.class);
        Mockito.verifyNoMoreInteractions(doctorDao, modelMapper);

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnUpdatedThrowException() {

        Assertions.assertThrows(Exception.class, () -> doctorService.update(0, DOCTOR_DTO_IN));

        Mockito.verify(doctorDao, Mockito.times(0))
                .save(Mockito.any(DoctorEntity.class));
        Mockito.verify(doctorDao, Mockito.times(0)).
                findById(Mockito.any(Integer.class));
        Mockito.verify(modelMapper, Mockito.times(0))
                .map(DOCTOR_DTO_IN, DoctorEntity.class);
        Mockito.verifyNoMoreInteractions(doctorDao, modelMapper);

    }

    @Test
    public void givenDoctorId_whenGetById_thenReturnDtoOut() {
        Mockito.when(doctorDao.findById(Mockito.any(Integer.class)))
                .thenReturn(Optional.of(SAVED_DOCTOR_ENTITY));

        DoctorDtoOut foundDoctorDtoOut = doctorService.getById(1);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(foundDoctorDtoOut);
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getId(), foundDoctorDtoOut.getId());
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getExpertiseId(), foundDoctorDtoOut.getExpertiseId());
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getFirstName(), foundDoctorDtoOut.getFirstName());
            Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getLastName(), foundDoctorDtoOut.getLastName());
        });

        Mockito.verify(doctorDao, Mockito.times(1))
                .findById(Mockito.any(Integer.class));
        Mockito.verifyNoMoreInteractions(doctorDao);
    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() {
        Mockito.when(doctorDao.findAll()).thenReturn(List.of(SAVED_DOCTOR_ENTITY));

        for (DoctorDtoOut doctorDtoOut : doctorService.getAll()) {
            Assertions.assertAll(() -> {
                Assertions.assertNotNull(doctorDtoOut);
                Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getId(), doctorDtoOut.getId());
                Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getExpertiseId(), doctorDtoOut.getExpertiseId());
                Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getFirstName(), doctorDtoOut.getFirstName());
                Assertions.assertEquals(SAVED_DOCTOR_DTO_OUT.getFirstName(), doctorDtoOut.getLastName());
            });
        }

        Mockito.verify(doctorDao, Mockito.times(1))
                .findAll();
        Mockito.verifyNoMoreInteractions(doctorDao);
    }

    @Test
    public void whenCount_thenReturnDtoOutsCount() {
        Mockito.when(doctorDao.count()).thenReturn(1L);

        long count = doctorService.count();

        Assertions.assertEquals(1, count);

        Mockito.verify(doctorDao, Mockito.times(1)).count();
        Mockito.verifyNoMoreInteractions(doctorDao);
    }

    @Test
    public void givenDoctorId_whenDeleteById() {
        Mockito.doNothing().when(doctorDao).deleteById(Mockito.any(Integer.class));

        doctorService.deleteById(1);

        Mockito.verify(doctorDao, Mockito.times(1)).deleteById(1);
        Mockito.verifyNoMoreInteractions(doctorDao);

    }

}
