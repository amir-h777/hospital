package com.routaa.hospital.bl.doctor.repository.dao;

import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.entity.DoctorEntity;
import com.routaa.hospital.entity.ExpertiseEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class DoctorDaoTest {

    private DoctorEntity DOCTOR_ENTITY;

    private final DoctorDao doctorDao;
    private final ExpertiseDao expertiseDao;

    @Autowired
    public DoctorDaoTest(DoctorDao doctorDao, ExpertiseDao expertiseDao){
        this.doctorDao = doctorDao;
        this.expertiseDao = expertiseDao;
    }

    public void preSetUp(){
        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity savedExpertise = expertiseDao.save(expertise);

        DOCTOR_ENTITY.setExpertiseId(savedExpertise.getId());
    }

    @BeforeEach
    public void setUp(){
        DOCTOR_ENTITY = new DoctorEntity();
        DOCTOR_ENTITY.setFirstName("TestDoctor");
        DOCTOR_ENTITY.setLastName("TestDoctor");
        preSetUp();
    }

    @Test
    public void givenDoctor_whenSave_thenReturnSavedDoctor(){
        DoctorEntity savedDoctorEntity = doctorDao.save(DOCTOR_ENTITY);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(savedDoctorEntity);
            Assertions.assertNotEquals(0,savedDoctorEntity.getId());
            Assertions.assertNotEquals(0,savedDoctorEntity.getExpertiseId());
            Assertions.assertEquals("TestDoctor", savedDoctorEntity.getFirstName());
            Assertions.assertEquals("TestDoctor", savedDoctorEntity.getLastName());
        });
    }

    @Test
    public void givenDoctor_whenUpdate_thenReturnUpdatedDoctor(){
        DoctorEntity savedDoctorEntity = doctorDao.save(DOCTOR_ENTITY);
        savedDoctorEntity.setFirstName("UpdatedDoctor");
        DoctorEntity updatedDoctorEntity = doctorDao.save(savedDoctorEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(updatedDoctorEntity);
            Assertions.assertEquals(savedDoctorEntity.getId(), updatedDoctorEntity.getId());
            Assertions.assertEquals(savedDoctorEntity.getExpertiseId(), updatedDoctorEntity.getExpertiseId());
            Assertions.assertEquals("UpdatedDoctor", updatedDoctorEntity.getFirstName());
            Assertions.assertEquals("TestDoctor", updatedDoctorEntity.getLastName());
        });
    }

    @Test
    public void givenDoctorId_whenFindById_thenReturnDoctors(){
        DoctorEntity savedDoctorEntity = doctorDao.save(DOCTOR_ENTITY);
        DoctorEntity foundDoctorEntity;
        if(doctorDao.findById(savedDoctorEntity.getId()).isPresent()){
            foundDoctorEntity = doctorDao.findById(savedDoctorEntity.getId()).get();

            Assertions.assertAll(() -> {
                Assertions.assertNotNull(foundDoctorEntity);
                Assertions.assertEquals(savedDoctorEntity.getId(), foundDoctorEntity.getId());
                Assertions.assertEquals(savedDoctorEntity.getExpertiseId(), foundDoctorEntity.getExpertiseId());
                Assertions.assertEquals("TestDoctor", foundDoctorEntity.getFirstName());
                Assertions.assertEquals("TestDoctor", foundDoctorEntity.getLastName());
            });

        }

    }

    @Test
    public void whenFindAll_thenReturnDoctorsIterable(){
        doctorDao.save(DOCTOR_ENTITY);
        doctorDao.save(DOCTOR_ENTITY);

        for (DoctorEntity doctor : doctorDao.findAll()) {
            Assertions.assertAll(() -> {
                Assertions.assertNotNull(doctor);
                Assertions.assertNotEquals(0, doctor.getId());
                Assertions.assertNotEquals(0, doctor.getExpertiseId());
                Assertions.assertEquals("TestDoctor", doctor.getFirstName());
                Assertions.assertEquals("TestDoctor", doctor.getLastName());
            });
        }

    }

    @Test
    public void whenCount_thenReturnDoctorsCount(){
        doctorDao.save(DOCTOR_ENTITY);
        doctorDao.save(DOCTOR_ENTITY);
        long count = doctorDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenDoctorsId_whenDeleteById(){
        DoctorEntity savedDoctorEntity = doctorDao.save(DOCTOR_ENTITY);

        Assertions.assertNotNull(savedDoctorEntity);

        doctorDao.deleteById(savedDoctorEntity.getId());

        Assertions.assertEquals(Optional.empty(),doctorDao.findById(savedDoctorEntity.getId()));
    }

}
