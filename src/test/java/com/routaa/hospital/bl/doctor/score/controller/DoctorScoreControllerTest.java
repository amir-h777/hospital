package com.routaa.hospital.bl.doctor.score.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.doctor.model.DoctorDtoIn;
import com.routaa.hospital.bl.doctor.model.DoctorDtoOut;
import com.routaa.hospital.bl.doctor.score.model.DoctorScoreDtoIn;
import com.routaa.hospital.bl.doctor.score.model.DoctorScoreDtoOut;
import com.routaa.hospital.bl.doctor.score.repository.service.DoctorScoreService;
import com.routaa.hospital.entity.DoctorScoreEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DoctorScoreController.class)
public class DoctorScoreControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DoctorScoreService doctorScoreService;

    private DoctorScoreEntity doctorScoreEntity;
    private DoctorScoreDtoOut doctorScoreDtoOut;
    private DoctorScoreDtoIn doctorScoreDtoIn;

    @BeforeEach
    public void setUp(){

        doctorScoreEntity = new DoctorScoreEntity();
        doctorScoreEntity.setId(1);
        doctorScoreEntity.setDoctorId(2);
        doctorScoreEntity.setScore((float) 12.34);

        doctorScoreDtoIn = new DoctorScoreDtoIn(doctorScoreEntity);
        doctorScoreDtoOut = new DoctorScoreDtoOut(doctorScoreEntity);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(doctorScoreService.create(Mockito.any(DoctorScoreDtoIn.class))).thenReturn(doctorScoreDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/doctor-score")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(doctorScoreDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.doctorId").value(2))
                .andExpect(jsonPath("$.score").value((float) 12.34));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        doctorScoreEntity.setScore((float)15.34);
        doctorScoreDtoIn = new DoctorScoreDtoIn(doctorScoreEntity);
        doctorScoreDtoOut = new DoctorScoreDtoOut(doctorScoreEntity);

        Mockito.when(doctorScoreService.update(Mockito.any(Integer.class), Mockito.any(DoctorScoreDtoIn.class))).thenReturn(doctorScoreDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/doctor-score/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(doctorScoreDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.doctorId").value(2))
                .andExpect(jsonPath("$.score").value((float) 15.34));

    }

    @Test
    public void givenDoctorScoreId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(doctorScoreService.getById(Mockito.any(Integer.class))).thenReturn(doctorScoreDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/doctor-score/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.doctorId").value(2))
                .andExpect(jsonPath("$.score").value((float) 12.34));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(doctorScoreService.getAll()).thenReturn(List.of(doctorScoreDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/doctor-scores")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(doctorScoreService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/doctor-scores/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenDoctorScoreId_whenDelete() throws Exception{

        Mockito.doNothing().when(doctorScoreService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/doctor-score/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
