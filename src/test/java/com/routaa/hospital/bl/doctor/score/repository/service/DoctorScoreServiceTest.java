package com.routaa.hospital.bl.doctor.score.repository.service;

import com.routaa.hospital.bl.doctor.model.DoctorDtoOut;
import com.routaa.hospital.bl.doctor.score.model.DoctorScoreDtoIn;
import com.routaa.hospital.bl.doctor.score.model.DoctorScoreDtoOut;
import com.routaa.hospital.bl.doctor.score.repository.dao.DoctorScoreDao;
import com.routaa.hospital.entity.DoctorEntity;
import com.routaa.hospital.entity.DoctorScoreEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class DoctorScoreServiceTest {

    @Mock
    private DoctorScoreDao doctorScoreDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private DoctorScoreService doctorScoreService;

    private DoctorScoreEntity doctorScoreEntity;
    private DoctorScoreDtoIn doctorScoreDtoIn;

    @BeforeEach
    public void setUp() {
        doctorScoreEntity = new DoctorScoreEntity();
        doctorScoreEntity.setId(1);
        doctorScoreEntity.setDoctorId(2);
        doctorScoreEntity.setScore((float) 12.34);

        doctorScoreDtoIn = new DoctorScoreDtoIn(doctorScoreEntity);
    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnSavedDtoOut() {

        Mockito.when(modelMapper.map(doctorScoreDtoIn, DoctorScoreEntity.class)).thenReturn(doctorScoreEntity);
        Mockito.when(doctorScoreDao.save(doctorScoreEntity)).thenReturn(doctorScoreEntity);

        DoctorScoreDtoOut dtoOut = doctorScoreService.create(doctorScoreDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });
    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnUpdatedDtoOut() {
        doctorScoreEntity.setScore((float) 14.56);
        doctorScoreDtoIn = new DoctorScoreDtoIn(doctorScoreEntity);

        Mockito.when(modelMapper.map(doctorScoreDtoIn, DoctorScoreEntity.class)).thenReturn(doctorScoreEntity);
        Mockito.when(doctorScoreDao.save(doctorScoreEntity)).thenReturn(doctorScoreEntity);
        Mockito.when(doctorScoreDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(doctorScoreEntity));

        DoctorScoreDtoOut updatedDtoOut = doctorScoreService.update(1, doctorScoreDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals((float) 14.56, (float) updatedDtoOut.getScore());
        });

    }

    @Test
    public void givenDoctorScoreId_whenGetById_thenReturnDtoOut(){
        Mockito.when(doctorScoreDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(doctorScoreEntity));

        DoctorScoreDtoOut foundDtoOut = doctorScoreService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(doctorScoreDao.findAll()).thenReturn(List.of(doctorScoreEntity));

        List<DoctorScoreDtoOut> dtoOutList = doctorScoreService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(doctorScoreDao.count()).thenReturn(1L);

        long count = doctorScoreService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenDoctorScoreId_whenDeleteById(){
        Mockito.doNothing().when(doctorScoreDao).deleteById(Mockito.any(Integer.class));

        doctorScoreService.deleteById(1);

        Mockito.verify(doctorScoreDao, Mockito.times(1)).deleteById(1);

    }

}
