package com.routaa.hospital.bl.doctor.score.repository.dao;

import com.routaa.hospital.bl.doctor.repository.dao.DoctorDao;
import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.entity.DoctorEntity;
import com.routaa.hospital.entity.DoctorScoreEntity;
import com.routaa.hospital.entity.ExpertiseEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class DoctorScoreDaoTest {

    private DoctorScoreEntity doctorScoreEntity;

    private DoctorScoreDao doctorScoreDao;
    private DoctorDao doctorDao;
    private ExpertiseDao expertiseDao;

    @Autowired
    public DoctorScoreDaoTest(DoctorScoreDao doctorScoreDao, DoctorDao doctorDao, ExpertiseDao expertiseDao){
        this.doctorScoreDao = doctorScoreDao;
        this.doctorDao = doctorDao;
        this.expertiseDao = expertiseDao;
    }

    public void preSetUp(){
        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved = expertiseDao.save(expertise);

        DoctorEntity doctor = new DoctorEntity();
        doctor.setExpertiseId(saved.getId());
        DoctorEntity saved1 = doctorDao.save(doctor);

        doctorScoreEntity.setDoctorId(saved1.getId());
    }

    @BeforeEach
    public void setUp(){
        doctorScoreEntity = new DoctorScoreEntity();
        doctorScoreEntity.setScore((float)12.34);
        preSetUp();
    }

    @Test
    public void givenDoctorScore_whenSave_thenReturnSavedDoctorScore(){
        DoctorScoreEntity saved = doctorScoreDao.save(doctorScoreEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getId());
        });
    }

    @Test
    public void givenDoctorScore_whenUpdate_thenReturnUpdatedDoctorScore(){
        DoctorScoreEntity saved = doctorScoreDao.save(doctorScoreEntity);
        saved.setScore((float) 15.34);
        DoctorScoreEntity updated = doctorScoreDao.save(saved);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(saved.getScore(), updated.getScore());
            Assertions.assertEquals(updated.getId(), saved.getId());
        });
    }

    @Test
    public void givenDoctorScoreId_whenFindById_thenReturnDoctorScores(){
        DoctorScoreEntity saved = doctorScoreDao.save(doctorScoreEntity);
        DoctorScoreEntity found = doctorScoreDao.findById(saved.getId()).get();

        Assertions.assertNotNull(found);
    }

    @Test
    public void whenFindAll_thenReturnDoctorScores(){
        doctorScoreDao.save(doctorScoreEntity);
        doctorScoreDao.save(doctorScoreEntity);
        Iterable<DoctorScoreEntity> iterable = doctorScoreDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnDoctorScoresCount(){
        doctorScoreDao.save(doctorScoreEntity);
        doctorScoreDao.save(doctorScoreEntity);
        long count = doctorScoreDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenDoctorScoresId_whenDeleteById(){
        DoctorScoreEntity saved = doctorScoreDao.save(doctorScoreEntity);

        Assertions.assertNotNull(saved);

        doctorScoreDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),doctorScoreDao.findById(saved.getId()));
    }

}
