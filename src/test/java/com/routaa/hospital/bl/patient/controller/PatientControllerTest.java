package com.routaa.hospital.bl.patient.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.patient.model.PatientDtoIn;
import com.routaa.hospital.bl.patient.model.PatientDtoOut;
import com.routaa.hospital.bl.patient.repository.service.PatientService;
import com.routaa.hospital.bl.room.model.RoomDtoIn;
import com.routaa.hospital.bl.room.model.RoomDtoOut;
import com.routaa.hospital.entity.PatientEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PatientController.class)
public class PatientControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PatientService patientService;

    private PatientEntity patientEntity;
    private PatientDtoIn patientDtoIn;
    private PatientDtoOut patientDtoOut;

    @BeforeEach
    public void setUp(){

        patientEntity = new PatientEntity();
        patientEntity.setId(1);
        patientEntity.setHospitalId(2);
        patientEntity.setHospitalSectionId(3);
        patientEntity.setDoctorId(4);
        patientEntity.setRoomId(5);
        patientEntity.setFirstName("TestPatient");
        patientEntity.setLastName("TestPatient");
        patientEntity.setIllness("TestIllness");

        patientDtoIn = new PatientDtoIn(patientEntity);
        patientDtoOut = new PatientDtoOut(patientEntity);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(patientService.create(Mockito.any(PatientDtoIn.class))).thenReturn(patientDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/patient")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(patientDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.hospitalSectionId").value(3))
                .andExpect(jsonPath("$.doctorId").value(4))
                .andExpect(jsonPath("$.roomId").value(5))
                .andExpect(jsonPath("$.firstName").value("TestPatient"))
                .andExpect(jsonPath("$.lastName").value("TestPatient"))
                .andExpect(jsonPath("$.illness").value("TestIllness"));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        patientEntity.setFirstName("UpdatedPatient");
        patientDtoIn = new PatientDtoIn(patientEntity);
        patientDtoOut = new PatientDtoOut(patientEntity);

        Mockito.when(patientService.update(Mockito.any(Integer.class), Mockito.any(PatientDtoIn.class))).thenReturn(patientDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/patient/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(patientDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.hospitalSectionId").value(3))
                .andExpect(jsonPath("$.doctorId").value(4))
                .andExpect(jsonPath("$.roomId").value(5))
                .andExpect(jsonPath("$.firstName").value("UpdatedPatient"))
                .andExpect(jsonPath("$.lastName").value("TestPatient"))
                .andExpect(jsonPath("$.illness").value("TestIllness"));

    }

    @Test
    public void givenPatientId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(patientService.getById(Mockito.any(Integer.class))).thenReturn(patientDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/patient/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.hospitalSectionId").value(3))
                .andExpect(jsonPath("$.doctorId").value(4))
                .andExpect(jsonPath("$.roomId").value(5))
                .andExpect(jsonPath("$.firstName").value("TestPatient"))
                .andExpect(jsonPath("$.lastName").value("TestPatient"))
                .andExpect(jsonPath("$.illness").value("TestIllness"));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(patientService.getAll()).thenReturn(List.of(patientDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/patients")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(patientService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/patients/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenPatientId_whenDelete() throws Exception{

        Mockito.doNothing().when(patientService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/patient/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
