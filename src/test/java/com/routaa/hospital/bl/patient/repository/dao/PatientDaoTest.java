package com.routaa.hospital.bl.patient.repository.dao;

import com.routaa.hospital.bl.doctor.repository.dao.DoctorDao;
import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.bl.hospital.section.repository.dao.HospitalSectionDao;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.bl.room.repository.dao.RoomDao;
import com.routaa.hospital.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class PatientDaoTest {

    private PatientEntity patientEntity;

    private PatientDao patientDao;
    private HospitalDao hospitalDao;
    private HospitalTypeDao hospitalTypeDao;
    private HospitalSectionDao hospitalSectionDao;
    private ExpertiseDao expertiseDao;
    private DoctorDao doctorDao;
    private RoomDao roomDao;

    @Autowired
    public PatientDaoTest(PatientDao patientDao, HospitalDao hospitalDao, HospitalTypeDao hospitalTypeDao
            , HospitalSectionDao hospitalSectionDao, ExpertiseDao expertiseDao
            , DoctorDao doctorDao, RoomDao roomDao){

        this.patientDao = patientDao;
        this.hospitalDao = hospitalDao;
        this.hospitalTypeDao = hospitalTypeDao;
        this.hospitalSectionDao = hospitalSectionDao;
        this.expertiseDao = expertiseDao;
        this.doctorDao = doctorDao;
        this.roomDao = roomDao;
    }

    public void preSetUp(){
        HospitalTypeEntity hospitalType = new HospitalTypeEntity();
        HospitalTypeEntity saved = hospitalTypeDao.save(hospitalType);

        HospitalEntity hospital = new HospitalEntity();
        hospital.setHospitalTypeId(saved.getId());
        HospitalEntity saved1 = hospitalDao.save(hospital);

        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved2 = expertiseDao.save(expertise);

        HospitalSectionEntity hospitalSection = new HospitalSectionEntity();
        hospitalSection.setExpertiseId(saved2.getId());
        hospitalSection.setHospitalId(saved1.getId());
        HospitalSectionEntity saved3 = hospitalSectionDao.save(hospitalSection);

        DoctorEntity doctor = new DoctorEntity();
        doctor.setExpertiseId(saved2.getId());
        DoctorEntity saved4 = doctorDao.save(doctor);

        RoomEntity room = new RoomEntity();
        room.setHospitalId(saved1.getId());
        room.setHospitalSectionId(saved3.getId());
        room.setDoctorId(saved4.getId());
        RoomEntity saved5 = roomDao.save(room);

        patientEntity.setHospitalId(saved1.getId());
        patientEntity.setHospitalSectionId(saved3.getId());
        patientEntity.setDoctorId(saved4.getId());
        patientEntity.setRoomId(saved5.getId());

    }

    @BeforeEach
    public void setUp(){
        patientEntity = new PatientEntity();
        patientEntity.setFirstName("TestPatient");
        patientEntity.setLastName("TestPatient");
        patientEntity.setIllness("TestIllness");
        preSetUp();
    }

    @Test
    public void givenPatient_whenSave_thenReturnSavedPatient(){
        PatientEntity saved = patientDao.save(patientEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getId());
        });
    }

    @Test
    public void givenPatient_whenUpdate_thenReturnUpdatedPatient(){
        PatientEntity saved = patientDao.save(patientEntity);
        saved.setFirstName("UpdatedPatient");
        PatientEntity updated = patientDao.save(saved);

        Assertions.assertAll(() -> {
            Assertions.assertEquals("UpdatedPatient", updated.getFirstName());
            Assertions.assertEquals(updated.getId(), saved.getId());
        });
    }

    @Test
    public void givenPatientId_whenFindById_thenReturnPatients(){
        PatientEntity saved = patientDao.save(patientEntity);
        PatientEntity found = patientDao.findById(saved.getId()).get();

        Assertions.assertNotNull(found);
    }

    @Test
    public void whenFindAll_thenReturnPatients(){
        patientDao.save(patientEntity);
        patientDao.save(patientEntity);
        Iterable<PatientEntity> iterable = patientDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnPatientsCount(){
        patientDao.save(patientEntity);
        patientDao.save(patientEntity);
        long count = patientDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenPatientsId_whenDeleteById(){
        PatientEntity saved = patientDao.save(patientEntity);

        Assertions.assertNotNull(saved);

        patientDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),patientDao.findById(saved.getId()));
    }

}
