package com.routaa.hospital.bl.patient.repository.service;

import com.routaa.hospital.bl.patient.model.PatientDtoIn;
import com.routaa.hospital.bl.patient.model.PatientDtoOut;
import com.routaa.hospital.bl.patient.repository.dao.PatientDao;
import com.routaa.hospital.bl.room.model.RoomDtoOut;
import com.routaa.hospital.entity.PatientEntity;
import com.routaa.hospital.entity.RoomEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class PatientServiceTest {

    @Mock
    private PatientDao patientDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private PatientService patientService;

    private PatientEntity patientEntity;
    private PatientDtoIn patientDtoIn;

    @BeforeEach
    public void setUp(){
        patientEntity = new PatientEntity();
        patientEntity.setId(1);
        patientEntity.setHospitalId(2);
        patientEntity.setHospitalSectionId(3);
        patientEntity.setDoctorId(4);
        patientEntity.setRoomId(5);
        patientEntity.setFirstName("TestPatient");
        patientEntity.setLastName("TestPatient");
        patientEntity.setIllness("TestIllness");

        patientDtoIn = new PatientDtoIn(patientEntity);
    }

    @Test
    public void givenPatient_whenCreate_thenReturnSavedDtoOut() {

        Mockito.when(modelMapper.map(patientDtoIn, PatientEntity.class)).thenReturn(patientEntity);
        Mockito.when(patientDao.save(patientEntity)).thenReturn(patientEntity);

        PatientDtoOut dtoOut = patientService.create(patientDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });
    }

    @Test
    public void givenPatient_whenUpdate_thenReturnUpdatedDtoOut() {

        patientEntity.setFirstName("UpdatedPatient");
        patientDtoIn = new PatientDtoIn(patientEntity);

        Mockito.when(modelMapper.map(patientDtoIn, PatientEntity.class)).thenReturn(patientEntity);
        Mockito.when(patientDao.save(patientEntity)).thenReturn(patientEntity);
        Mockito.when(patientDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(patientEntity));


        PatientDtoOut updatedDtoOut = patientService.update(1, patientDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals("UpdatedPatient", updatedDtoOut.getFirstName());
        });

    }

    @Test
    public void givenPatientId_whenGetById_thenReturnDtoOut(){
        Mockito.when(patientDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(patientEntity));

        PatientDtoOut foundDtoOut = patientService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(patientDao.findAll()).thenReturn(List.of(patientEntity));

        List<PatientDtoOut> dtoOutList = patientService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(patientDao.count()).thenReturn(1L);

        long count = patientService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenPatientId_whenDeleteById(){
        Mockito.doNothing().when(patientDao).deleteById(Mockito.any(Integer.class));

        patientService.deleteById(1);

        Mockito.verify(patientDao, Mockito.times(1)).deleteById(1);

    }

}
