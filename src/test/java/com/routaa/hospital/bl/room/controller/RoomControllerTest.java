package com.routaa.hospital.bl.room.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.routaa.hospital.bl.doctor.model.DoctorDtoIn;
import com.routaa.hospital.bl.doctor.model.DoctorDtoOut;
import com.routaa.hospital.bl.room.model.RoomDtoIn;
import com.routaa.hospital.bl.room.model.RoomDtoOut;
import com.routaa.hospital.bl.room.repository.service.RoomService;
import com.routaa.hospital.entity.RoomEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RoomController.class)
public class RoomControllerTest {

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private MockMvc mockMvc;

    /** @noinspection SpringJavaInjectionPointsAutowiringInspection*/
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RoomService roomService;

    private RoomEntity roomEntity;
    private RoomDtoIn roomDtoIn;
    private RoomDtoOut roomDtoOut;

    @BeforeEach
    public void setUp(){

        roomEntity = new RoomEntity();
        roomEntity.setId(1);
        roomEntity.setHospitalId(2);
        roomEntity.setHospitalSectionId(3);
        roomEntity.setDoctorId(4);
        roomEntity.setName("TestRoom");
        roomEntity.setNumber(999);

        roomDtoIn = new RoomDtoIn(roomEntity);
        roomDtoOut = new RoomDtoOut(roomEntity);

    }

    @Test
    public void givenDtoIn_whenCreate_thenReturnDtoOut() throws Exception {

        Mockito.when(roomService.create(Mockito.any(RoomDtoIn.class))).thenReturn(roomDtoOut);

        ResultActions resultActions = mockMvc.perform(post("/room")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(roomDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.hospitalSectionId").value(3))
                .andExpect(jsonPath("$.doctorId").value(4))
                .andExpect(jsonPath("$.name").value("TestRoom"))
                .andExpect(jsonPath("$.number").value(999));

    }

    @Test
    public void givenDtoIn_whenUpdate_thenReturnDtoOut() throws Exception{

        roomEntity.setName("UpdatedRoom");
        roomDtoIn = new RoomDtoIn(roomEntity);
        roomDtoOut = new RoomDtoOut(roomEntity);

        Mockito.when(roomService.update(Mockito.any(Integer.class), Mockito.any(RoomDtoIn.class))).thenReturn(roomDtoOut);

        ResultActions resultActions = mockMvc.perform(put("/room/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(roomDtoIn)));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.hospitalSectionId").value(3))
                .andExpect(jsonPath("$.doctorId").value(4))
                .andExpect(jsonPath("$.name").value("UpdatedRoom"))
                .andExpect(jsonPath("$.number").value(999));

    }

    @Test
    public void givenRoomId_whenGetById_thenReturnDtoOut() throws Exception{

        Mockito.when(roomService.getById(Mockito.any(Integer.class))).thenReturn(roomDtoOut);

        ResultActions resultActions = mockMvc.perform(get("/room/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hospitalId").value(2))
                .andExpect(jsonPath("$.hospitalSectionId").value(3))
                .andExpect(jsonPath("$.doctorId").value(4))
                .andExpect(jsonPath("$.name").value("TestRoom"))
                .andExpect(jsonPath("$.number").value(999));

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts() throws Exception{

        Mockito.when(roomService.getAll()).thenReturn(List.of(roomDtoOut));

        ResultActions resultActions = mockMvc.perform(get("/rooms")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1));

    }

    @Test
    public void whenCount_thenReturnDtoOutCounts() throws Exception{

        Mockito.when(roomService.count()).thenReturn(5L);

        ResultActions resultActions = mockMvc.perform(get("/rooms/count")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(5));

    }

    @Test
    public void givenRoomId_whenDelete() throws Exception{

        Mockito.doNothing().when(roomService).deleteById(Mockito.any(Integer.class));

        ResultActions resultActions = mockMvc.perform(delete("/room/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE));


        resultActions.andDo(print())
                .andExpect(status().isOk());
    }

}
