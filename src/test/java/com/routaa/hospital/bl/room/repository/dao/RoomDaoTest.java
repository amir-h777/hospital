package com.routaa.hospital.bl.room.repository.dao;

import com.routaa.hospital.bl.doctor.repository.dao.DoctorDao;
import com.routaa.hospital.bl.expertise.repository.dao.ExpertiseDao;
import com.routaa.hospital.bl.hospital.repository.dao.HospitalDao;
import com.routaa.hospital.bl.hospital.section.repository.dao.HospitalSectionDao;
import com.routaa.hospital.bl.hospital.type.repository.dao.HospitalTypeDao;
import com.routaa.hospital.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class RoomDaoTest {

    private RoomEntity roomEntity;

    private RoomDao roomDao;
    private HospitalDao hospitalDao;
    private HospitalTypeDao hospitalTypeDao;
    private HospitalSectionDao hospitalSectionDao;
    private ExpertiseDao expertiseDao;
    private DoctorDao doctorDao;

    @Autowired
    public RoomDaoTest(RoomDao roomDao, HospitalDao hospitalDao, HospitalTypeDao hospitalTypeDao
            , HospitalSectionDao hospitalSectionDao, ExpertiseDao expertiseDao, DoctorDao doctorDao){

        this.roomDao = roomDao;
        this.hospitalDao = hospitalDao;
        this.hospitalTypeDao = hospitalTypeDao;
        this.hospitalSectionDao = hospitalSectionDao;
        this.expertiseDao = expertiseDao;
        this.doctorDao = doctorDao;

    }

    public void preSetUp(){
        HospitalTypeEntity hospitalType = new HospitalTypeEntity();
        HospitalTypeEntity saved = hospitalTypeDao.save(hospitalType);

        HospitalEntity hospital = new HospitalEntity();
        hospital.setHospitalTypeId(saved.getId());
        HospitalEntity saved1 = hospitalDao.save(hospital);

        ExpertiseEntity expertise = new ExpertiseEntity();
        ExpertiseEntity saved2 = expertiseDao.save(expertise);

        HospitalSectionEntity hospitalSection = new HospitalSectionEntity();
        hospitalSection.setExpertiseId(saved2.getId());
        hospitalSection.setHospitalId(saved1.getId());
        HospitalSectionEntity saved3 = hospitalSectionDao.save(hospitalSection);

        DoctorEntity doctor = new DoctorEntity();
        doctor.setExpertiseId(saved2.getId());
        DoctorEntity saved4 = doctorDao.save(doctor);

        roomEntity.setHospitalId(saved1.getId());
        roomEntity.setHospitalSectionId(saved3.getId());
        roomEntity.setDoctorId(saved4.getId());
    }

    @BeforeEach
    public void setUp(){
        roomEntity = new RoomEntity();
        roomEntity.setName("TestRoom");
        roomEntity.setNumber(9999);
        preSetUp();
    }

    @Test
    public void givenRoom_whenSave_thenReturnSavedRoom(){
        RoomEntity saved = roomDao.save(roomEntity);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(saved);
            Assertions.assertNotEquals(0,saved.getId());
        });
    }

    @Test
    public void givenRoom_whenUpdate_thenReturnUpdatedRoom(){
        RoomEntity saved = roomDao.save(roomEntity);
        saved.setName("UpdatedRoom");
        RoomEntity updated = roomDao.save(saved);

        Assertions.assertAll(() -> {
            Assertions.assertEquals("UpdatedRoom", updated.getName());
            Assertions.assertEquals(updated.getId(), saved.getId());
        });
    }

    @Test
    public void givenRoomId_whenFindById_thenReturnRooms(){
        RoomEntity saved = roomDao.save(roomEntity);
        RoomEntity found = roomDao.findById(saved.getId()).get();

        Assertions.assertNotNull(found);
    }

    @Test
    public void whenFindAll_thenReturnRooms(){
        roomDao.save(roomEntity);
        roomDao.save(roomEntity);
        Iterable<RoomEntity> iterable = roomDao.findAll();

        Assertions.assertNotNull(iterable);
    }

    @Test
    public void whenCount_thenReturnRoomsCount(){
        roomDao.save(roomEntity);
        roomDao.save(roomEntity);
        long count = roomDao.count();

        Assertions.assertNotEquals(0,count);
    }

    @Test
    public void givenRoomsId_whenDeleteById(){
        RoomEntity saved = roomDao.save(roomEntity);

        Assertions.assertNotNull(saved);

        roomDao.deleteById(saved.getId());

        Assertions.assertEquals(Optional.empty(),roomDao.findById(saved.getId()));
    }

}
