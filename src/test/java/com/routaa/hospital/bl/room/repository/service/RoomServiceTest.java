package com.routaa.hospital.bl.room.repository.service;

import com.routaa.hospital.bl.doctor.model.DoctorDtoOut;
import com.routaa.hospital.bl.room.model.RoomDtoIn;
import com.routaa.hospital.bl.room.model.RoomDtoOut;
import com.routaa.hospital.bl.room.repository.dao.RoomDao;
import com.routaa.hospital.entity.DoctorEntity;
import com.routaa.hospital.entity.RoomEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class RoomServiceTest {

    @Mock
    private RoomDao roomDao;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private RoomService roomService;

    private RoomEntity roomEntity;
    private RoomDtoIn roomDtoIn;

    @BeforeEach
    public void setUp(){
        roomEntity = new RoomEntity();
        roomEntity.setId(1);
        roomEntity.setHospitalId(2);
        roomEntity.setHospitalSectionId(3);
        roomEntity.setDoctorId(4);
        roomEntity.setNumber(99999);
        roomEntity.setName("TestRoom");

        roomDtoIn = new RoomDtoIn(roomEntity);
    }

    @Test
    public void givenRoom_whenCreate_thenReturnSavedDtoOut() {

        Mockito.when(modelMapper.map(roomDtoIn, RoomEntity.class)).thenReturn(roomEntity);
        Mockito.when(roomDao.save(roomEntity)).thenReturn(roomEntity);

        RoomDtoOut dtoOut = roomService.create(roomDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOut);
            Assertions.assertEquals(1, dtoOut.getId());
        });
    }

    @Test
    public void givenRoom_whenUpdate_thenReturnUpdatedDtoOut() {

        roomEntity.setName("UpdatedRoom");
        roomDtoIn = new RoomDtoIn(roomEntity);

        Mockito.when(modelMapper.map(roomDtoIn, RoomEntity.class)).thenReturn(roomEntity);
        Mockito.when(roomDao.save(roomEntity)).thenReturn(roomEntity);
        Mockito.when(roomDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(roomEntity));

        RoomDtoOut updatedDtoOut = roomService.update(1, roomDtoIn);

        Assertions.assertAll(() -> {
            Assertions.assertEquals(1, updatedDtoOut.getId());
            Assertions.assertEquals("UpdatedRoom", updatedDtoOut.getName());
        });

    }

    @Test
    public void givenRoomId_whenGetById_thenReturnDtoOut(){
        Mockito.when(roomDao.findById(Mockito.any(Integer.class))).thenReturn(Optional.of(roomEntity));

        RoomDtoOut foundDtoOut = roomService.getById(1);

        Assertions.assertNotNull(foundDtoOut);

    }

    @Test
    public void whenGetAll_thenReturnDtoOuts(){
        Mockito.when(roomDao.findAll()).thenReturn(List.of(roomEntity));

        List<RoomDtoOut> dtoOutList = roomService.getAll();

        Assertions.assertAll(() -> {
            Assertions.assertNotNull(dtoOutList);
            Assertions.assertEquals(1, dtoOutList.size());
        });

    }

    @Test
    public void whenCount_thenReturnDtoOutsCount(){
        Mockito.when(roomDao.count()).thenReturn(1L);

        long count = roomService.count();

        Assertions.assertEquals(1, count);
    }

    @Test
    public void givenRoomId_whenDeleteById(){
        Mockito.doNothing().when(roomDao).deleteById(Mockito.any(Integer.class));

        roomService.deleteById(1);

        Mockito.verify(roomDao, Mockito.times(1)).deleteById(1);

    }

}
